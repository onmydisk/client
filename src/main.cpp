/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "maindialog.h"
#include <qtsingleapplication.h>

//#include <QApplication>
#include <QSystemTrayIcon>

int main(int argc, char *argv[])
{

    QtSingleApplication instance("onmydisk", argc, argv);
    if (instance.isRunning())
    {
        instance.sendMessage("Wake up!");
        return 0;
    }


    instance.setStyle(QStyleFactory::create("Fusion"));
    instance.setApplicationName("On My Disk Client");
    instance.setOrganizationName("On My Disk");
    instance.setQuitOnLastWindowClosed(false);


    MainDialog d;
    d.show();
    instance.setActivationWindow(&d);

    QObject::connect(&instance, SIGNAL(messageReceived(const QString&)),
             &d, SLOT(ipcMessage(const QString&)));

    return instance.exec();
}
