


Name:           onmydisk
Version:        0.3
Release:        1
Summary:        On My Disk desktop client

License:        OBL
URL:            https://onmydisk.com
Source0:        %{name}-%{version}.tar.gz

BuildRequires: desktop-file-utils


%description
On My Disk desktop client.



%prep
%setup -q


%build
mkdir build
pushd build
qmake-qt5 ../src/onmydisk.pro "CONFIG+=DEPLOY" "BUILD_VERSION=%{version}-%{release}"
make %{?_smp_mflags}
popd


%install
pushd build
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps
install -m 755 -p ../bin/linux/onmydisk $RPM_BUILD_ROOT/usr/bin/onmydisk
install -m 655 -p ../src/onmydisk.svg  $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/onmydisk.svg
desktop-file-install                         \
--delete-original                            \
--dir=%{buildroot}%{_datadir}/applications   \
../src/onmydisk.desktop

%files
/usr/bin/onmydisk
%{_datadir}/icons/hicolor/scalable/apps/onmydisk.svg
%{_datadir}/applications/onmydisk.desktop

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%changelog
* Wed May 21 2014 Kozinov Ivan
- First release.
