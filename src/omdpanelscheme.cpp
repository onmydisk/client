#include "omdpanelscheme.h"


namespace QSint
{


const char* ActionPanelOnMyDiskStyle =

    "QToolbar {"
        "background: #1d2d42;"
    "}"

    "QSint--ActionGroup QFrame[class='header'] {"
        "background: transparent;"
        "border: 1px solid #cccccc;"
        "border-radius: 2px;"
    "}"

    "QSint--ActionGroup QFrame[class='header']:hover {"
        "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 rgba(249,253,255,100), stop: 0.5 rgba(234,247,255,20), stop: 1 rgba(249,253,255,100));"
        "border: 1px solid #cccccc;"
        "border-radius: 2px;"
    "}"

    "QSint--ActionGroup QToolButton[class='header'] {"
        "text-align: left;"
        "color: #1d2d42;"
        "background-color: transparent;"
        "border: 1px solid transparent;"
        "font-size: 12px;"
    "}"

    "QSint--ActionGroup QToolButton[class='header']:hover {"
        "color: #0071bc;"
    "}"

    "QSint--ActionGroup QFrame[class='content'] {"
        "background-color: transparent;"
        "color: #ffffff;"
    "}"

    "QSint--ActionGroup QToolButton[class='action'] {"
        "background-color: transparent;"
        "border: 1px solid transparent;"
        "color: #ffffff;"
        "text-align: left;"
    "}"

    "QSint--ActionGroup QToolButton[class='action']:enabled {"
            "color: #000000;"
     "}"


    "QSint--ActionGroup QToolButton[class='action']:!enabled {"
        "color: #666666;"
    "}"

    "QSint--ActionGroup QToolButton[class='action']:hover {"
        "color: #0071bc;"
        "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 rgba(249,253,255,100), stop: 0.5 rgba(234,247,255,20), stop: 1 rgba(249,253,255,100));"
        "border: 1px solid #cccccc;"
        "border-radius: 2px;"

     "}"



;

/*    Some original patterns

"QSint--ActionGroup QToolButton[class='action']:on {"
"background-color: #ddeeff;"
"color: #006600;"
"}"
0071bc
    "QSint--ActionGroup QToolButton[class='action']:focus {"
        "border: 1px dotted black;"
    "}"

*/

OnMyDiskPanelScheme::OnMyDiskPanelScheme() : ActionPanelScheme()
{
  headerSize = 25;
  headerAnimation = false;

  //headerLabelScheme.iconSize = 22;

  headerButtonFold = QPixmap(":icons/resource/omd/Fold.png");
  headerButtonFoldOver = QPixmap(":icons/resource/omd/FoldOver.png");
  headerButtonUnfold = QPixmap(":icons/resource/omd/Unfold.png");
  headerButtonUnfoldOver = QPixmap(":icons/resource/omd/UnfoldOver.png");
  headerButtonSize = QSize(17,17);

  groupFoldSteps = 20;
  groupFoldDelay = 15;
  groupFoldThaw = true;
  groupFoldEffect = SlideFolding;

  actionStyle = QString(ActionPanelOnMyDiskStyle);
}


}

