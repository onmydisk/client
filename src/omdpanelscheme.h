#ifndef OnMyDiskPanelScheme_H
#define OnMyDiskPanelScheme_H

#include <QSint>


namespace QSint
{


/**
    \brief OnMyDisk color scheme for ActionPanel and ActionGroup.
    \since 0.2

    \image html ActionPanel3.png Example of the scheme
*/
class OnMyDiskPanelScheme : public ActionPanelScheme
{
public:
    OnMyDiskPanelScheme();

    static ActionPanelScheme* defaultScheme()
    {
        static OnMyDiskPanelScheme scheme;
        return &scheme;
    }
};


}


#endif // OnMyDiskPanelScheme_H
