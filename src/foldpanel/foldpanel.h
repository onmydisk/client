/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef FOLDABLEPANEL_H
#define FOLDABLEPANEL_H

#include <QWidget>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QAction>
#include <QToolButton>

namespace Ui {
class FoldablePanel;
}

class ActionButton;

class FoldablePanel : public QWidget
{
    Q_OBJECT
    friend class ActionButton;
private:
    bool m_folded;
    bool m_mouseOver;
    bool m_animationActive;
    int m_visibilityEdge;
    QPropertyAnimation *heightAnimation;
    QPropertyAnimation *opacityAnimation;
    QGraphicsOpacityEffect* opacityEffect ;

public:
    explicit FoldablePanel(const QString &title,  QWidget *parent = 0, bool fold = false);
    static int overhead;
    bool isFolded() { return m_folded; }
    void addWidget(QWidget *widget);
    void addAction(QAction *action);
    int headerHeight();
    void fold();
    void unfold();
    ~FoldablePanel();
protected:
    void updateIcon();
    void setFoldState(bool fold);
    bool eventFilter(QObject *obj, QEvent *event);
    virtual void resizeEvent(QResizeEvent *event);

private:
    Ui::FoldablePanel *ui;

private slots:
    void animationCompleted();
signals:
    void animationStarted(bool &fold);
    void animationFinished(bool &fold);

};


//Hidden class for action buttons
class ActionButton: public QToolButton
{
    Q_OBJECT
public:
    explicit ActionButton(QAction *action, QWidget *parent = 0);
    virtual QSize sizeHint() const;
    virtual QSize minimumSizeHint() const;
    ~ActionButton();
};

#endif // FOLDABLEPANEL_H
