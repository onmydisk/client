/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "foldpanel.h"
#include "ui_foldpanel.h"
#include <QToolButton>
#include <QResizeEvent>

#include <QDebug>

ActionButton::ActionButton(QAction *action, QWidget *parent ):
QToolButton(parent)
{
        setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        setDefaultAction(action);
}

QSize ActionButton::sizeHint() const
{
        ensurePolished();
        int w = 0;
        int h = 0;
        QStyleOptionToolButton style_option;
        initStyleOption(&style_option);
        QString title(text());
        bool empty = title.isEmpty();
        if (empty)
        {
            title = QString::fromLatin1("BUTTON");
        }
        QFontMetrics fm = fontMetrics();
        QSize text_size = fm.size(Qt::TextShowMnemonic, title);

        if(!empty || !w)
        {
            w += text_size.width();
        }

        if(!empty || !h)
        {
            h = qMax(h, text_size.height());
        }

        style_option.rect.setSize(QSize(w, h));

        if (!icon().isNull())
        {
            int ih = style_option.iconSize.height();
            int iw = style_option.iconSize.width() + 4;
            w += iw;
            h = qMax(h, ih);
        }

        if (menu())
        {
            w += style()->pixelMetric(QStyle::PM_MenuButtonIndicator, &style_option, this);
        }

        h += 16;
        w += 8;

        QSize sizeHint = (style()->sizeFromContents(QStyle::CT_PushButton, &style_option, QSize(w, h), this).expandedTo(QApplication::globalStrut()));
        return sizeHint;
}

QSize ActionButton::minimumSizeHint() const
{
      return sizeHint();
}


ActionButton::~ActionButton()
{

}

//Foldable panel

int FoldablePanel::overhead = 100;


FoldablePanel::FoldablePanel(const QString &title,  QWidget *parent, bool folded ) :
    QWidget(parent),
    ui(new Ui::FoldablePanel)
{
    ui->setupUi(this);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    ui->contents->setAttribute(Qt::WA_NoSystemBackground, true);
    ui->contents->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    ui->headerLabel->setText(title);

    // set  background to translucent
    ui->contents->setAttribute(Qt::WA_TranslucentBackground, true);
    heightAnimation = new QPropertyAnimation(this, "geometry");
    opacityEffect =  new QGraphicsOpacityEffect(this);
    ui->contents->setGraphicsEffect(opacityEffect);
    opacityEffect->setOpacity(1.0);

    opacityAnimation = new QPropertyAnimation(this);
    opacityAnimation->setTargetObject(opacityEffect);
    opacityAnimation->setPropertyName("opacity");
    heightAnimation->setDuration(200);
    opacityAnimation->setDuration(100);
    heightAnimation->setEasingCurve(QEasingCurve::OutQuad);
    opacityAnimation->setEasingCurve(QEasingCurve::OutQuad);
    connect(heightAnimation, SIGNAL(finished()), this, SLOT(animationCompleted()));

    m_mouseOver = false;
    m_animationActive = false;
    m_folded = folded;
    m_visibilityEdge = ui->header->height() + 2;

    if (m_folded)
    {
      ui->verticalLayout->setSpacing(0);
      opacityEffect->setOpacity(0.001);
      ui->contents->setVisible(false);
    } else
    {
       ui->verticalLayout->setSpacing(4);
    }
    updateIcon();
    // hook up all events
    qApp->installEventFilter(this);
}

bool FoldablePanel::eventFilter(QObject *obj, QEvent *event)
{
  if (obj == ui->headerLabel || obj == ui->headerIcon )
  {
     switch (event->type())
     {
        case QEvent::MouseButtonPress:
                    if (!m_animationActive) setFoldState(!m_folded);
                    break;

        case QEvent::Enter:
                    m_mouseOver =  (obj == ui->headerIcon) ;
                    updateIcon();
                    break;

        case QEvent::Leave:
                    m_mouseOver = false;
                    updateIcon();
                    break;

        default: break;
  }

 }

 return QWidget::eventFilter(obj, event);
}


void FoldablePanel::setFoldState(bool fold)
{
  emit animationStarted(fold);

 if (m_folded != fold && !m_animationActive)
 {
   // qDebug() << ui->headerLabel->text() << fold;
    m_animationActive = true;

    QRect g( geometry());
    heightAnimation->setStartValue(g);
    if (fold)
    {
      g.setHeight(ui->header->height());
      heightAnimation->setEndValue(g);
      opacityAnimation->setStartValue(1.0);
      opacityAnimation->setEndValue(0.001);
      ui->verticalLayout->setSpacing(0);
      opacityAnimation->start();
    } else
    {
      ui->verticalLayout->setSpacing(4);
      g.setHeight(parentWidget()->height() - FoldablePanel::overhead);
      heightAnimation->setEndValue(g);
    }


    heightAnimation->start();
 }
}


int FoldablePanel::headerHeight()
{
    return ui->header->height();
}





void FoldablePanel::addWidget(QWidget *widget)
{
    ui->contents->layout()->addWidget(widget);
}

void FoldablePanel::addAction(QAction *action)
{
    ActionButton *b = new  ActionButton(action, this);
    ui->contents->layout()->addWidget(b);
}


void FoldablePanel::fold()
{
    setFoldState(true);
}

void FoldablePanel::unfold()
{
    setFoldState(false);

}



void FoldablePanel::resizeEvent(QResizeEvent * event)
{

    if (! m_folded)
    {
        if (event->size().height() <= m_visibilityEdge && ! ui->contents->isHidden())
        {
            ui->contents->setVisible( false );
            if (!m_animationActive)
            {
                m_folded = true;
                updateIcon();
            }

        }
    } else
    {
        if (event->size().height() >= m_visibilityEdge &&  ui->contents->isHidden())
        {
            ui->contents->setVisible( true );
            opacityAnimation->setStartValue(0.001);
            opacityAnimation->setEndValue(1.0);
            opacityAnimation->start();

       }

    }

    event->accept();


}


void FoldablePanel::animationCompleted()
{
    m_animationActive = false;
    m_folded = ! m_folded;

    ui->contents->setVisible( ! m_folded );
    parentWidget()->layout()->update();
    parentWidget()->update();
    updateIcon();

    emit animationFinished(m_folded);
}

void FoldablePanel::updateIcon()
{
    if (m_mouseOver)
    {
      if (! m_folded)
        ui->headerIcon->setPixmap(QPixmap(":/icons/FoldOver"));
      else
        ui->headerIcon->setPixmap(QPixmap(":/icons/UnfoldOver"));
    } else
    {
      if (! m_folded)
        ui->headerIcon->setPixmap(QPixmap(":/icons/Fold"));
      else
        ui->headerIcon->setPixmap(QPixmap(":/icons/Unfold"));
    }
}


FoldablePanel::~FoldablePanel()
{
    delete heightAnimation;
    delete opacityAnimation;
    delete opacityEffect;
    delete ui;
}


