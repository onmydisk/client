INCLUDEPATH	+= $$PWD
DEPENDPATH      += $$PWD


SOURCES +=  $$PWD/foldpanel.cpp

HEADERS  += $$PWD/foldpanel.h

FORMS += $$PWD/foldpanel.ui

RESOURCES += $$PWD/foldpanel.qrc


win32:contains(TEMPLATE, lib):contains(CONFIG, shared) {
    DEFINES += VFS_EXPORT=__declspec(dllexport)
}
