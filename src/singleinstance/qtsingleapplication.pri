INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD


SOURCES += $$PWD/qtsingleapplication.cpp \
     $$PWD/qtlocalpeer.cpp
HEADERS += $$PWD/qtsingleapplication.h \
      $$PWD/qtlocalpeer.h

win32:contains(TEMPLATE, lib):contains(CONFIG, shared) {
    DEFINES += QTSINGLEAPPLICATION_EXPORT=__declspec(dllexport)
}
