INCLUDEPATH	+= $$PWD
DEPENDPATH      += $$PWD
QT *= network


SOURCES +=  $$PWD/blockreader.cpp \
    $$PWD/blockwriter.cpp \
    $$PWD/qstat.cpp \
    $$PWD/vfsrequest.cpp \
    $$PWD/vfsresponse.cpp \
    $$PWD/sslhelper.cpp \
    $$PWD/vfsengine.cpp

HEADERS  += $$PWD/blockreader.h \
    $$PWD/blockwriter.h \
    $$PWD/qstat.h \
    $$PWD/vfsrequest.h \
    $$PWD/vfsresponse.h \
    $$PWD/common.h \
    $$PWD/sslhelper.h \
    $$PWD/vfsengine.h


win32:contains(TEMPLATE, lib):contains(CONFIG, shared) {
    DEFINES += VFS_EXPORT=__declspec(dllexport)
}
