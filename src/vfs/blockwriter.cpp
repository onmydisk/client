/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "blockwriter.h"

BlockWriter::BlockWriter(QIODevice *io, int v)
{
    buffer.open(QIODevice::WriteOnly);
    this->io = io;
    _stream.setVersion(v);
    _stream.setDevice(&buffer);
    // Placeholder for the size. We will get the value
    // at the end.
    _stream << quint64(0);
}


BlockWriter::~BlockWriter()
{
    // Write the real size.
    _stream.device()->seek(0);
    _stream << (quint64) buffer.size();

    // Flush to the device.
    io->write(buffer.buffer());
    //qDebug() << "real sent size: " << sentSize;
    //qDebug() << "message size: " << buffer.size();
    io->waitForBytesWritten(5000);
}

QDataStream& BlockWriter::stream()
{
    return _stream;
}
