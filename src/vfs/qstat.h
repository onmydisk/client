/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef QSTAT_H
#define QSTAT_H
#include "sys/stat.h"
#include <QFileInfo>
#include <QFile>
#include <QDateTime>
#include <QDataStream>
#include <QDebug>

class VFSFileInfo
{

public:
    VFSFileInfo(QFileInfo* fileInfo);
    VFSFileInfo();
    void Fill(QFileInfo* fileInfo);
#ifndef WIN32
    void fillStatBuf(struct stat *statbuf);
#endif
    friend QDebug & operator<<(QDebug & dbg, const VFSFileInfo &QStatData);
    friend QDataStream & operator<<(QDataStream & ds, const VFSFileInfo &QStatData);
    friend QDataStream & operator>>(QDataStream & ds, VFSFileInfo &QStatData);
    QString fileName;
    bool isExists;
    bool isDir;
    bool isFile;
    bool isReadable;
    bool isWritable;
    bool isExecutable;
    QFile::Permissions perms;
    qint32 ownrerId;
    qint32 groupId;
    qint64 size;
    qint64 atime;
    qint64 mtime;
    qint64 ctime;
};

inline QDebug & operator<<(QDebug & dbg, const VFSFileInfo &VFSFileInfoData)
{
    dbg << QString("QStat: {");
    dbg << QString("fileName") << VFSFileInfoData.fileName;
    dbg << QString("Exists") << VFSFileInfoData.isExists;
    dbg << QString("isDir") << VFSFileInfoData.isDir;
    dbg << QString("isFile") << VFSFileInfoData.isFile;
    dbg << QString("isReadable") << VFSFileInfoData.isReadable;
    dbg << QString("isWritable") << VFSFileInfoData.isWritable;
    dbg << QString("isExecutable") << VFSFileInfoData.isExecutable;
    dbg << QString("atime") << VFSFileInfoData.atime;
    dbg << QString("ctime") << VFSFileInfoData.ctime;
    dbg << QString("mtime") << VFSFileInfoData.mtime;
    dbg << QString("size") << VFSFileInfoData.size;
    dbg << QString("ownrerId") << VFSFileInfoData.ownrerId;
    dbg << QString("groupId") << VFSFileInfoData.groupId;
    dbg << QString("}");
    return dbg;
}

inline QDataStream & operator<<(QDataStream & ds, const VFSFileInfo &VFSFileInfoData)
{
    ds << VFSFileInfoData.fileName << VFSFileInfoData.isExists << VFSFileInfoData.isDir << VFSFileInfoData.isFile << VFSFileInfoData.isReadable << VFSFileInfoData.isWritable << VFSFileInfoData.isExecutable << VFSFileInfoData.atime << VFSFileInfoData.ctime << VFSFileInfoData.mtime << VFSFileInfoData.size << VFSFileInfoData.ownrerId << VFSFileInfoData.groupId;
    return ds;
}

inline QDataStream & operator>>(QDataStream & ds, VFSFileInfo &VFSFileInfoData)
{
    ds >> VFSFileInfoData.fileName >> VFSFileInfoData.isExists >> VFSFileInfoData.isDir >> VFSFileInfoData.isFile >> VFSFileInfoData.isReadable >> VFSFileInfoData.isWritable >> VFSFileInfoData.isExecutable >> VFSFileInfoData.atime >> VFSFileInfoData.ctime >> VFSFileInfoData.mtime >> VFSFileInfoData.size >> VFSFileInfoData.ownrerId >> VFSFileInfoData.groupId;
    return ds;
}



#endif // QSTAT_H
