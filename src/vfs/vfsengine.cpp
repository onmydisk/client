/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "vfsengine.h"

VFSEngine::VFSEngine(QString sharePath, QString keyDir, QString clientType, QObject *parent) :
    QThread(parent), socket(this), pingTimer(this), helper(keyDir)
{
    moveToThread(this);
#ifdef QT5
    version = QDataStream::Qt_5_0;
#else
    version = QDataStream::Qt_4_8;
#endif
    share_path = sharePath;
    client_type = clientType;
    buffer.open(QIODevice::ReadWrite);
    _stream.setVersion(version);
    _stream.setDevice(&buffer);
    blockSize = 0;
    isConnected = false;
    isReconnecting = false;

    //response handling
    s_buffer.open(QIODevice::ReadWrite);
    s_stream.setVersion(version);
    s_stream.setDevice(&s_buffer);
    s_blockSize = 0;

    share_id = 0;
    deleted_id = 0;
    connectionCount = 10;
}

void VFSEngine::setSharePath(QString sharePath)
{
    if (isConnected) Disconnect();
    share_path = sharePath;
    doReconnect(true);
}

VFSEngine::~VFSEngine()
{
    this->Disconnect();
}

void VFSEngine::addOpTimer( QTimer *timer, int op_code)
{
    op_timers.insert(op_code, timer);
    connect(timer, SIGNAL(timeout()), &mapper, SLOT(map()));
    mapper.setMapping(timer, op_code);

}

void VFSEngine::run()
{
    //protocol-version specific timings
    addOpTimer(new QTimer(this), CMD_CONNECT);
    addOpTimer(new QTimer(this), CMD_PING);
    addOpTimer(new QTimer(this), CMD_RESCAN);
    addOpTimer(new QTimer(this), CMD_DELETE);

    connect(&mapper, SIGNAL(mapped(int)), this, SLOT(responseTimout(int)));

    //ping timer works on it's own since it is not an operation timout timer.
    connect(&pingTimer, SIGNAL(timeout()), this, SLOT(doPing()));

    connect(&socket, SIGNAL(encrypted()), this, SLOT(connectionEncrypted()));
    connect(&socket, SIGNAL(readyRead()), this, SLOT(handleSystemResponse()));
    connect(&socket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslError(QList<QSslError>)));
    connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(connectionError(QAbstractSocket::SocketError)));
    for(int i = 0; i < connectionCount; i++)
    {
        QSslSocket* dataSocket = new QSslSocket();
        connect(dataSocket, SIGNAL(readyRead()),this, SLOT(handleDataRequest()));
        connect(dataSocket, SIGNAL(encrypted()), this, SLOT(dataConnectionEncrypted()));
        connect(dataSocket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslError(QList<QSslError>)));
        connect(dataSocket, SIGNAL(disconnected()), this, SLOT(localShareDisconnected()));
        connect(dataSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(tX(qint64)));
        connect(dataSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(connectionError(QAbstractSocket::SocketError)));
        dataSockets.append(dataSocket);
    }
    exec();
}

void VFSEngine::cleanup(bool reconnect)
{
    share_id = 0;
    isConnected = false;
    disconnectRequested = !reconnect;

    foreach (QTimer *t, op_timers)
    {
        t->stop();
    }
    pingTimer.stop();
    socket.abort();
    socket.close();
    for(int i = 0; i < connectionCount; i++)
    {
        dataSockets.at(i)->abort();
        dataSockets.at(i)->close();
    }

    emit Disconnected(!disconnectRequested);

}

void VFSEngine::setCredentials(QString user, QString password)
{
    if (isConnected) Disconnect();
    qDebug() << "Requesting certificate";

    if (! helper.caCertificateExists())
    {
        helper.SaveCaToFile();
    }
    if (! helper.GenerateKeyToFile())
    {
        qDebug() << "Cannot generate pkey to file";
        cleanup(false);
        emit ConnectionError("Cannot generate private key.", false);
        return;
    }


    if (! helper.RequestCertToFile(user, password))
    {
        qDebug() << "Error requesting a certificate";
        cleanup(false);
        emit ConnectionError("Error requesting a certificate.", false);
        return;
    }
     qDebug() << "Certificate received.";

     doReconnect(true, true);

}

void VFSEngine::Connect()
{

  if (! isConnected )
  {
    if (helper.privateKeyExists() && helper.certificateExists())
    {
                qDebug() << "Loading certificate";
                helper.loadPrivateKey();
                helper.loadCertificate();
    }
    else
    {
                qDebug() << "Certificate does not exist, generate new";
                //to avoid spurious reconnects
                op_timers[CMD_CONNECT]->stop();
                emit WantNewCert();
                return;
    }


     QSslCertificate cert = helper.getCertificate();
     emit CertificateLoaded(cert);

     //we have certificate and private key, so we are ready to connect
     //ok, initiate mount process here
     // localShareConnected event should be invoked
     socket.setCaCertificates(helper.getCaCertificates());
     socket.setLocalCertificate(cert);
     socket.setPrivateKey(helper.getPrivateKey());
     socket.connectToHostEncrypted("cloud.onmydisk.com", 4000);
     op_timers[CMD_CONNECT]->start(CONNECT_TIMEOUT);
  }

}

void VFSEngine::Disconnect()
{
    cleanup(false);
}

void VFSEngine::tX(qint64 bytesSent)
{
    emit TX(bytesSent);
}

void VFSEngine::rX(qint64 bytesSent)
{
    emit RX(bytesSent);
}

void VFSEngine::doReconnect(bool delayed, bool forced)
{
   if (forced || (! isReconnecting))
   {
     qDebug() << "reconnecting...";
     isReconnecting = true;
     cleanup(true);
     if (delayed)
        QTimer::singleShot(randInt(1000, 3000), this, SLOT(Connect()));
     else
        Connect();
   }

}

void VFSEngine::responseTimout(int response_type)
{

    switch (response_type)
    {
     case CMD_RESCAN:
     {
        op_timers[CMD_RESCAN]->stop();
        emit responseReceived(CMD_RESCAN, ERR_TIMEOUT);
        break;
     }

     case CMD_CONNECT:
     {
        op_timers[CMD_CONNECT]->stop();
        qDebug() << "Connect timeout, try again";
        doReconnect(false, true);
        emit ConnectionError(tr("Connection timeout, trying again..."), true);

        break;
     }
     case CMD_PING:
     {

        op_timers[CMD_PING]->stop();
        qDebug() << "Ping timeout";
        doReconnect(false);

        break;
     }
        default: break;
    }
}

void VFSEngine::localShareDisconnected()
{
  //  cleanup();
    bool reconnect =  !disconnectRequested;
    if (isConnected)
    {
        cleanup(reconnect);
    }

    if (!isReconnecting && reconnect)
    {
        qDebug() << "Reconnecting from disconnected";
        doReconnect(true);
    }
}

void VFSEngine::connectionEncrypted()
{
    qDebug() << "Encrypted.";
    if (deleted_id != 0)
    {
        deleteDeleted();

    } else
    {
        // perform CONNECT command

        sendRequest(CMD_CONNECT);
    }
}

void VFSEngine::dataConnectionEncrypted()
{
    op_timers[CMD_CONNECT]->stop();
    // set connected
    isReconnecting = false;
    disconnectRequested = false;
    isConnected = true;
    emit Connected();
    pingTimer.start(PING_INTERVAL);
}


void VFSEngine::doPing()
{
    sendRequest(CMD_PING);
}

void VFSEngine::connectResponse(int retCode)
{

    if (retCode > 0)
    {
        share_id = retCode;
        for(int i = 0; i < connectionCount; i++)
        {
            dataSockets.at(i)->setCaCertificates(helper.getCaCertificates());
            dataSockets.at(i)->setLocalCertificate(helper.getCertificate());
            dataSockets.at(i)->setPrivateKey(helper.getPrivateKey());
            dataSockets.at(i)->setSocketOption(QAbstractSocket::LowDelayOption,1);
            dataSockets.at(i)->connectToHostEncrypted("cloud.onmydisk.com", share_id);
        }
        //setStatus("Ready...");
    }
    else
    {
        isConnected = false;

        switch (retCode)
        {
            case ERR_CERT_REMOVED:
            {
                //this should create a new cert
                 qDebug() << "Certificate removed, reconnecting";
                //helper.reset();
                //this shuld start reconnect
                cleanup(false);
                emit ConnectionError(tr("Certificate is removed, create new."), false);
                break;
            }
            case ERR_NO_USER:
            {
                //this should create a new cert
                cleanup(false);
                //this shuld start reconnect
                emit ConnectionError(tr("Account is removed, acquire a new certificate."), false);
                break;
            }

            case ERR_CLIENT_OUTDATED:
            {
                cleanup(false);
                emit ConnectionError(tr("Outdated client program, update needed."), false);
                break;
            }
            case ERR_ENOTEMPTY:
            case ERR_EXISTS:
            {
                doReconnect(true, true);
                emit ConnectionError(tr("Resource is busy. Retrying..."), true);
                break;
            }
            case ERR_FUSE_NOT_STARTED:
            {
                doReconnect(true, true);
                emit ConnectionError(tr("Client process failed to start. Retrying..."), true);
                break;
            }
            default:
            {
                doReconnect(true, true);
                emit ConnectionError(tr("Unknown error. Retrying..."), true);
                break;
            }

        }

    }


}


int VFSEngine::randInt(int low, int high)
{
    // Random number between low and high
    return qrand() % ((high + 1) - low) + low;
}


void VFSEngine::connectionError( QAbstractSocket::SocketError error )
{

    qDebug() << "Connection error: " << error << ", reconnecting : " << isReconnecting;


    switch (error)
    {
      case QAbstractSocket::AddressInUseError:
          emit ConnectionError( tr("Address is in use, retrying..."), true);
          break;
      case QAbstractSocket::ConnectionRefusedError:
          doReconnect(true);
          emit ConnectionError( tr("Connection refused, retrying..."), true);
          break;
      case QAbstractSocket::HostNotFoundError:
          doReconnect(true);
          emit ConnectionError( tr( "Host not found, retrying..."), true);
          break;
      case QAbstractSocket::RemoteHostClosedError:
          doReconnect(true);
          emit ConnectionError(tr("Server closed connection, reconnecting..."), true);
          break;

       default:
          doReconnect(true);
          emit ConnectionError( tr("Connection error, retrying..."), true );
          break;
      }

}


void VFSEngine::sslError(QList<QSslError> errList)
{
    bool expired = false;
    QString err = "";
    foreach (QSslError e, errList)
    {
           err += (err == "") ? e.errorString() : ", " + e.errorString();
           if (e == QSslError::CertificateExpired)
           {
               expired = true;
           }
    }

    qDebug() << err;


    if (expired)
    {
        helper.reset();
        doReconnect(true);
    }
     else
    {
        cleanup(false);
    }
    emit ConnectionError(tr("SSL error: %1").arg(err), expired);


}

void VFSEngine::handleDataRequest()
{
    QSslSocket* dataSocket = (QSslSocket*)sender();
    //this slot connected to dataSocket and it is main exchange point.
    /*
     * Here we need to get CMD_CODE and perform switch/case with
     * getting necessary data from socket, executing local VFS handler
     * and transmitting reply to socket
    */
//    QSslSocket* s = (QSslSocket*) sender();
    //qDebug() << "reading request...";
    VFSRequest request;
    //BlockReader is not working here, we need to implement fast reading chunks of data until full request recieved.
    //BlockReader(s, &requestSize).stream() >> request;
    //qDebug() << "Bytes in socket: " << s->bytesAvailable();
    buffer.seek(buffer.size());
    emit RX(dataSocket->bytesAvailable());
    buffer.write(dataSocket->read(dataSocket->bytesAvailable()));
    //qDebug() << "Bytes in buffer: " << buffer.size();
    //qDebug() << "Data in buffer: " << buffer.readAll();
    //qDebug() << "Bytes in socket: " << s->bytesAvailable();
    if (blockSize == 0)
    {
        //read size and avaliable data
        buffer.seek(0);
        _stream >> blockSize;
        //qDebug() << "Reading block size: " << blockSize;
    }
    //qDebug() << "Bytes in buffer: " << buffer.size();
    if (blockSize > 0 && buffer.size() >= (int) blockSize)
    {
        //qDebug() << "reading request";
        buffer.seek(sizeof(blockSize));
        _stream >> request;
        buffer.buffer().clear();
        //qDebug() << "Bytes in buffer: " << buffer.size();
        blockSize = 0;
        buffer.seek(0);
        //qDebug() << "done";
        //RX(requestSize);
        VFSResponse response = handleDataRequest(request);
        BlockWriter(dataSocket, version).stream() << response;
        //qDebug() << "response sent";
    }
}


//System communication handling
void VFSEngine::handleSystemResponse()
{


  VFSResponse response;

  s_buffer.seek(s_buffer.size());
  s_buffer.write(socket.read(socket.bytesAvailable()));

  if (s_blockSize == 0)
  {
      //read size and avaliable data
      s_buffer.seek(0);
      s_stream >> s_blockSize;
      //qDebug() << "Reading block size: " << blockSize;
  }
  //qDebug() << "Bytes in buffer: " << buffer.size();
  if (s_blockSize > 0 && s_buffer.size() >= (int) s_blockSize)
  {
      //qDebug() << "reading request";
      s_buffer.seek(sizeof(s_blockSize));
      s_stream >> response;
      s_buffer.buffer().clear();
      //qDebug() << "Bytes in buffer: " << buffer.size();
      s_blockSize = 0;
      s_buffer.seek(0);
      //qDebug() << "done";

    //reset operation timer
      if (op_timers.contains(response.opCode))
      {
          op_timers[response.opCode]->stop();
      }

    //handle response

     switch (response.opCode)
     {
        case CMD_CONNECT:
        {
            // set port and connect to it
            connectResponse(response.retCode);
            break;

        }
        case CMD_PING:
        {
            //perform reconnect if resource was unmounted during our sleep,
            if (response.retCode != OK_RESPONSE)
            {
                qDebug() << "Resource was unmount by server";
                doReconnect(true);
                emit ConnectionError(tr("Disconnected by server, reconnecting..."), true);
            }
            break;

        }
        case CMD_DELETE:
        {

            if (response.share_id == (int) share_id )
            {
                   helper.reset();
                   qDebug("Certificate deleted on request.");

            } else

            if (response.share_id == (int)  deleted_id )
            {
                   qDebug("Old certificate deleted.");
                   deleted_id = 0;
            }

            doReconnect(true, true);


         break;
        }

        default: break;
    }

     // pass response to upper layer
     emit responseReceived(response.opCode, response.retCode);
  }

}




void VFSEngine::deleteDeleted()
{
    if (deleted_id != 0)
    {
      VFSRequest request;
      request.cmd_type = CMD_DELETE;
      request.share_id  = deleted_id;
      op_timers[CMD_DELETE]->start(RESPONSE_TIMEOUT);
      BlockWriter(&socket,version).stream()  << request;
    }
}




//Sends request to server
void VFSEngine::sendRequest(int cmd_type)
{
  if (socket.isOpen())
  {
    VFSRequest request;
  //  qDebug() << "Sending request" << cmd_type;
    request.cmd_type = cmd_type;
    switch (cmd_type)
    {
        case CMD_CONNECT:
        {
            // set port and connect to it
            request.protocol_version = PROTOCOL_VERSION;
            request.protocol_stream_version = version;
            request.client_type = client_type;
            op_timers[CMD_CONNECT]->start(CONNECT_TIMEOUT);
            break;

        }
        case CMD_PING:
        {
            request.share_id = share_id;
            op_timers[CMD_PING]->start(PING_TIMEOUT);
            break;

        }
        case CMD_RESCAN:
        {
            // emit scan finished
            request.share_id = share_id;
            op_timers[CMD_RESCAN]->start(SCAN_TIMEOUT);
            break;
        }

        case CMD_DELETE:
        {
            // emit scan finished
            request.share_id = share_id;
            op_timers[CMD_DELETE]->start(RESPONSE_TIMEOUT);
            break;
        }
        default: break;
    }

    BlockWriter(&socket, version).stream()  << request;
   // socket.flush();
  }
  else
  { // some delayed actions

      if (cmd_type == CMD_DELETE && helper.certificateExists())
      {
        // delete this record on next connect
        deleted_id = helper.getSerialNumber(helper.getCertificate()) + USER_PORT_OFFSET;
        helper.reset();

      }

  }
}


VFSResponse VFSEngine::handleDataRequest(VFSRequest request)
{
    VFSResponse response;
    response.opCode = request.cmd_type;

    emit FilesAccessNotification(request.cmd_type, request.path);
    switch (request.cmd_type) {
    case CMD_PING:
    {
      //  qDebug() << "It is ping";
        response.retCode = 0;
      //  qDebug() << "ping handled";
        break;
    }
    case CMD_READDIR:
    {
      //  qDebug() << "It is readdir";
        handleReaddir(&response, &request);
      //  qDebug() << "readdir handled";
        break;
    }
    case CMD_GETATTR:
    {
      //  qDebug() << "It is getattr";
        handleGetattr(&request, &response);
      //  qDebug() << "getattr handled";
        break;
    }
    case CMD_MKDIR:
    {
      //  qDebug() << "It is mkdir";
        handleMkdir(&response, &request);
      //  qDebug() << "getattr handled";
        break;
    }
    case CMD_MKNODE:
    {
      //  qDebug() << "It is mknode";
        handleMknode(&request, &response);
      //  qDebug() << "getattr handled";
        break;
    }
    case CMD_OPEN:
    {
     //   qDebug() << "It is open";
        handleOpen(&response, &request);
      //  qDebug() << "open handled";
        break;
    }
    case CMD_RELEASE:
    {
      //  qDebug() << "It is release";
        handleRelease(&request, &response);
      //  qDebug() << "release handled";
        break;
    }
    case CMD_UNLINK:
    {
     //   qDebug() << "It is unlink";
        handleUnlink(&request, &response);
     //   qDebug() << "unlink handled";
        break;
    }
    case CMD_RMDIR:
    {
      //  qDebug() << "It is rmdir";
        hanldeRmdir(&request, &response);
      //  qDebug() << "rmdir handled";
        break;
    }
    case CMD_RENAME:
    {
      //  qDebug() << "It is rename";
        handleRename(&request, &response);
      //  qDebug() << "rename handled";
        break;
    }
    case CMD_READ:
    {
      //  qDebug() << "It is read";
        handleRead(&request, &response);
     //   qDebug() << "read handled";
        break;
    }
    case CMD_WRITE:
    {
      //  qDebug() << "It is write";
        handleWrite(&request, &response);
      //  qDebug() << "read handled";
        break;
    }
    case CMD_STATVFS:
    {
     //   qDebug() << "It is statvfs";
        handleStatVFS(&request, &response);
     //   qDebug() << "read handled";
        break;
    }
   default:
        qDebug() << "Unknown request";
        //unknown operation
        break;
    }
    //delay(1000);

    return response;
}

void VFSEngine::handleReaddir(VFSResponse* response, VFSRequest* request)
{
    QString dirPath = request->path;
    dirPath.prepend(share_path);
    qDebug() << dirPath;
    QDir d(dirPath);
   // qDebug() << "QDir created";
    if (d.exists(dirPath))
    {
        qDebug() << "dir exist";
        response->retCode = 0;
        QStringList localListing = QDir(dirPath).entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files);
        for(int i = 0; i < localListing.length(); i++)
        {
            QString fullPath = request->path;
            fullPath.prepend(share_path);
            QFileInfo fi(fullPath);
            VFSFileInfo qStat(&fi);
            qStat.fileName = localListing.at(i);
            response->listing.append(qStat);
        }
    }
    else
    {
        qDebug() << "dir doesn't exist";
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::handleGetattr(VFSRequest* request, VFSResponse* response)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);
    //qDebug() << fullPath;
    QFileInfo fi(fullPath);
   // qDebug() << "QFileInfo created";
    if (fi.exists())
    {
     //   qDebug() << "File exists";
        response->retCode = 0;
        response->qStat.Fill(&fi);
    }
    else
    {
    //  qDebug() << "handleGetattr: File doesn't exist";
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::handleMkdir(VFSResponse* response, VFSRequest* request)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);
   // qDebug() << fullPath;
    QDir d(fullPath);
   // qDebug() << "QFileInfo created";
    if (d.exists())
    {
        qDebug() << "File exists";
        response->retCode = ERR_EXISTS;
    }
    else
    {
        if (d.mkpath(fullPath))
        {
           // qDebug() << "handleMkdir: Created " << fullPath;
            response->retCode = 0;
        }
        else
        {
        //    qDebug() << "handleMkdir: Can not create " << fullPath;
            response->retCode = ERR_ACCESS;
        }
    }
}

void VFSEngine::handleMknode(VFSRequest* request, VFSResponse* response)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);
   // qDebug() << fullPath;
    QFile f(fullPath);
   // qDebug() << "QFileInfo created";
    if (f.exists())
    {
        qDebug() << "handleMknode: File exists";
        response->retCode = ERR_EXISTS;
    }
    else
    {
        if (f.open(QIODevice::WriteOnly))
        {
            f.close();
            if (request->qStat.isReadable)
            {
                f.setPermissions(f.permissions() | QFile::ReadOwner);
            }
            if (request->qStat.isWritable)
            {
                f.setPermissions(f.permissions() | QFile::WriteOwner);
            }
            if (request->qStat.isExecutable)
            {
                f.setPermissions(f.permissions() | QFile::ExeOwner);
            }
            response->retCode = 0;
            qDebug() << "handleMknode:" << fullPath;

        }
        else
        {
            qDebug() << "handleMknode: Can not open writeonly: " << fullPath;

            response->retCode = ERR_ACCESS;
        }
    }
}

void VFSEngine::handleOpen(VFSResponse* response, VFSRequest* request)
{

    QString fullPath = request->path;
    fullPath.prepend(share_path);
    //qDebug() << "Open" << fullPath;


    if (QFile::exists(fullPath))
    {
        QFile *f = new QFile(fullPath);

        //TODO: 1. open with real flags from request, if cannot open - leave it.
        QFile::OpenMode opMode = QFile::NotOpen;
        if (request->openAppend)
        {
            opMode |= QFile::Append;
        }
        if (request->openRO)
        {
            opMode |= QFile::ReadOnly;
        }
        if (request->openWO)
        {
            opMode |= QFile::WriteOnly;
        }
        if (request->openRW)
        {
            opMode |= QFile::ReadWrite;
        }
        if (f->open(opMode))
        {
            quint64 fh = f->handle();
            op_files.insert(fh, f);
            response->retCode = 0;
            response->fh = fh;
            // f.close();
            qDebug() << "Open:" << fullPath  << opMode << "fh: " << fh;
        }
        else
        {
            f->deleteLater();
            response->retCode = ERR_ACCESS;
        }
    }
    else
    {
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::handleRelease(VFSRequest* request, VFSResponse* response)
{
    /*
    QString fullPath = request->path;
    fullPath.prepend(share_path);
    */
    if ( op_files.contains(request->fh))
    {
        op_files[request->fh]->close();
        op_files[request->fh]->deleteLater();
        op_files.remove(request->fh);
        qDebug() << "Release " << request->fh;
    }
    response->retCode = 0;
}

void VFSEngine::handleUnlink(VFSRequest* request, VFSResponse* response)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);
    // qDebug() << fullPath;

    QFile f(fullPath);
    if (f.exists())
    {
        if (f.remove())
        {
            response->retCode = 0;
        }
        else
        {
            response->retCode = ERR_ACCESS;
        }
    }
    else
    {
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::hanldeRmdir(VFSRequest* request, VFSResponse* response)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);
//    qDebug() << fullPath;

    QDir d(fullPath);
    if (d.exists())
    {
        if (d.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files).count() > 0)
        {
            response->retCode = ERR_ENOTEMPTY;
        }
        else
        {
            if (d.rmdir(fullPath))
            {
                response->retCode = 0;
            }
            else
            {
                response->retCode = ERR_ACCESS;
            }
        }
    }
    else
    {
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::handleRename(VFSRequest* request, VFSResponse* response)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);

    QString newFullPath = request->newPath;

    newFullPath.prepend(share_path);
    qDebug() << "Rename: " << fullPath << "New: " << newFullPath ;
   // qDebug() << fullPath;
    QFileInfo oldInfo(fullPath);

    if (oldInfo.isFile())
    {
        //alexey: remove target file if exists
        if (QFile::exists(newFullPath))
        {
         // qDebug() << newFullPath << "exists - removing";
          QFile::remove(newFullPath);
        }

        QFile f(fullPath);
        if (f.exists())
        {
            if (f.rename(fullPath, newFullPath))
            {
             response->retCode = 0;
            }
            else
            {
                qDebug() << "handleRename: Can not rename, target file still exists:" << newFullPath;
                response->retCode = ERR_ACCESS;
            }
        }
        else
        {
            qDebug() << "handleRename: Original file does not exist:" << fullPath;
            response->retCode = ERR_ACCESS;
         }
    } else

    if (oldInfo.isDir())
    {

        QDir d(fullPath);
        if (d.exists())
        {
            if (d.rename(fullPath, newFullPath))
            {
                response->retCode = 0;
            }
            else
            {
                response->retCode = ERR_ACCESS;
            }
        }
    }
}

void VFSEngine::handleRead(VFSRequest* request, VFSResponse* response)
{
    /*
    QString fullPath = request->path;
    fullPath.prepend(share_path);
    */
    //qDebug() << "Read fh=" << request->fh << " offset: " << request->offset << " size: " << request->size;


    if ( op_files.contains(request->fh))
    {
        //quint64 offset = f.pos();
        op_files[request->fh]->seek(request->offset);
        response->data = op_files[request->fh]->read(request->size);
        //f.seek(offset);
        response->retCode = response->data.count();
        if (response->retCode == -1)
        {
            response->retCode = ERR_ACCESS;
        }
    }
    else
    {
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::handleWrite(VFSRequest *request, VFSResponse *response)
{

    /*
    QString fullPath = request->path;
    fullPath.prepend(share_path);
    */
   // qDebug() << "Write" << request->fh << request->offset;

    if ( op_files.contains(request->fh))
    {
        op_files[request->fh]->seek(request->offset);
        response->retCode = op_files[request->fh]->write(request->data, request->size);
        if (response->retCode == -1)
        {
            response->retCode = ERR_ACCESS;
        }
    }
    else
    {
        response->retCode = ERR_NOTFOUND;
    }
}

void VFSEngine::handleStatVFS(VFSRequest *request, VFSResponse *response)
{
    QString fullPath = request->path;
    fullPath.prepend(share_path);

#ifdef WIN32
    ULONGLONG free_bytes_available; /* for user - similar to bavail */
    ULONGLONG total_number_of_bytes;
    ULONGLONG total_number_of_free_bytes; /* for everyone - bfree */

    //wchar_t p[fullPath.length()];
    //int filled = fullPath.toWCharArray(p);
    QString drive = fullPath.left(1) + ":/";
    std::wstring ws = drive.toStdWString();
    const wchar_t * pszDrive = ws.c_str(); // pszDrive is the letter of the

    if (!GetDiskFreeSpaceEx (pszDrive,
                             (PULARGE_INTEGER) &free_bytes_available,
                             (PULARGE_INTEGER) &total_number_of_bytes,
                             (PULARGE_INTEGER) &total_number_of_free_bytes)) {
      response->retCode = ERR_ACCESS;
    }
    else
    {
          response->f_bsize = 4096;
          response->f_bsize = response->f_bsize;
          response->f_blocks = total_number_of_bytes / response->f_bsize;
          response->f_bfree = total_number_of_free_bytes / response->f_bsize;
          response->f_bavail = free_bytes_available / response->f_bsize;
          response->retCode = 0;
    }
#else
    struct statvfs * buff;
    extern int errno;

    if ( !(buff = (struct statvfs *)
            malloc(sizeof(struct statvfs)))) {
            perror ("Failed to allocate memory to buffer.");
            response->retCode = errno;
    }
    if (statvfs(fullPath.toLocal8Bit().constData(), buff) < 0) {
            perror("statvfs() has failed.");
            response->retCode = errno;
    } else {
            response->f_bavail = buff->f_bavail;
            response->f_bfree = buff->f_bfree;
            response->f_blocks = buff->f_blocks;
            response->f_bsize = buff->f_bsize;
            response->retCode = 0;
    }
    free(buff);
#endif
}
