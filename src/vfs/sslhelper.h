/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef SSLHELPER_H
#define SSLHELPER_H

#include <openssl/pem.h>
#include <openssl/conf.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#ifndef OPENSSL_NO_ENGINE
#include <openssl/engine.h>
#endif
#include <QSsl>
#include <QSslKey>
#include <QSslCertificate>
#include <QSslConfiguration>
#include <QFile>
#include <QDir>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QHostInfo>

#define caCertFileName "ca.cert"
#define keyFileName "private.key"
#define certFileName "user.crt"

class SslHelper: QObject
{
    Q_OBJECT

public:
    SslHelper();
    SslHelper(QString CertKeyPath);
    void SetCertKeyPath(QString CertKeyPath);
    QSslKey getPrivateKey();
    QSslCertificate getCertificate();
    QList<QSslCertificate> getCaCertificates();
    bool GenerateKeyToFile();
    bool RequestCertToFile(QString login, QString password);
    bool SaveCaToFile();
    bool privateKeyExists();
    bool certificateExists();
    bool caCertificateExists();
    bool loadPrivateKey();
    bool loadCertificate();
    QString GetSubjectNameCN();
    static int getSerialNumber(QSslCertificate cert);
    static void reset(QDir profilePath);
    void reset();

private:
    QDir profilePath;
    X509_REQ *x;
    EVP_PKEY *pk;
    QSslKey pkey;
    QSslCertificate cert;
    QByteArray signRequest;
    bool GenerateCertSignRequest();

private slots:
    void certSigned(QNetworkReply *reply);
};

#endif // SSLHELPER_H
