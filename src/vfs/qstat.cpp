/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "qstat.h"


VFSFileInfo::VFSFileInfo(QFileInfo *fileInfo)
{
    fileName = fileInfo->fileName();
    isExists = fileInfo->exists();
    isDir = fileInfo->isDir();
    isFile = fileInfo->isFile();
    ownrerId = fileInfo->ownerId();
    groupId = fileInfo->groupId();
    isReadable = fileInfo->isReadable();
    isWritable = fileInfo->isWritable();
    isExecutable = fileInfo->isExecutable();
    //permissions = fileInfo->permissions();
    atime = fileInfo->lastRead().toTime_t();
    mtime = fileInfo->lastModified().toTime_t();
    ctime = fileInfo->created().toTime_t();
    size = fileInfo->size();
}
#ifndef WIN32
void VFSFileInfo::fillStatBuf(struct stat *statbuf)
{
    if (isDir)
    {
        statbuf->st_mode = S_IFDIR;
    }
    if (isFile)
    {
        statbuf->st_mode = S_IFREG;
    }
    if (isReadable)
    {
        statbuf->st_mode |= S_IRUSR;
    }
    if (isWritable)
    {
        statbuf->st_mode |= S_IWUSR;
    }
    if (isExecutable)
    {
        statbuf->st_mode |= S_IXUSR;
    }
    timespec at = {atime, 0};
    statbuf->st_atim = at;
    timespec ct = {ctime, 0};
    statbuf->st_ctim = ct;
    timespec mt = {mtime, 0};
    statbuf->st_mtim = mt;

    statbuf->st_uid = 48; //response.qStat.ownrerId;
    statbuf->st_gid = 48; //response.qStat.groupId;

    statbuf->st_size = size;
    statbuf->st_blksize = 512;
}
#endif
void VFSFileInfo::Fill(QFileInfo *fileInfo)
{
    fileName = fileInfo->fileName();
    isExists = fileInfo->exists();
    isDir = fileInfo->isDir();
    isFile = fileInfo->isFile();
    ownrerId = fileInfo->ownerId();
    groupId = fileInfo->groupId();
    isReadable = fileInfo->isReadable();
    isWritable = fileInfo->isWritable();
    isExecutable = fileInfo->isExecutable();
    //permissions = fileInfo->permissions();
    atime = fileInfo->lastRead().toTime_t();
    mtime = fileInfo->lastModified().toTime_t();
    ctime = fileInfo->created().toTime_t();
    size = fileInfo->size();
}

VFSFileInfo::VFSFileInfo()
{
    isExists = false;
    isDir = false;
    isFile = false;
    ownrerId = 0;
    groupId = 0;
    //permissions = fileInfo->permissions();
    isReadable = false;
    isWritable = false;
    isExecutable = false;
    atime = 0;
    mtime = 0;
    ctime = 0;
    size = 0;
}

