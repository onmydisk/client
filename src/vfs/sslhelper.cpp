/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "sslhelper.h"
#ifdef WIN32
#undef X509_NAME					//Get rid of wincrypt.h constant, save a warning
#define X509_NAME X509_name_st		//Use OpenSSL struct for X509 names
#endif

SslHelper::SslHelper()
{

}

SslHelper::SslHelper(QString CertKeyPath)
{
    SetCertKeyPath(CertKeyPath);
}

void SslHelper::SetCertKeyPath(QString CertKeyPath)
{
    profilePath = QDir(CertKeyPath);
}

QSslKey SslHelper::getPrivateKey()
{
    return pkey;
}

QSslCertificate SslHelper::getCertificate()
{
    return cert;
}

QList<QSslCertificate> SslHelper::getCaCertificates()
{
    QList<QSslCertificate> cas = QSslCertificate::fromPath(profilePath.filePath("ca.cert"));
    return  cas;
}

bool SslHelper::GenerateKeyToFile()
{
    RSA *rsa;
    pk=EVP_PKEY_new();
    rsa=RSA_generate_key(4096,RSA_F4,NULL,NULL);
    EVP_PKEY_assign_RSA(pk,rsa);
    //save it to QSslKey
    BIO *bio = BIO_new(BIO_s_mem());
    PEM_write_bio_RSAPrivateKey(bio, rsa, (const EVP_CIPHER *)0, NULL, 0, 0, 0);
    QByteArray pem;
    char *data;
    long size = BIO_get_mem_data(bio, &data);
    pem = QByteArray(data, size);
    BIO_free(bio);
    pkey=QSslKey(pem,QSsl::Rsa);
    if (!pkey.isNull())
    {
        QFile keyFile(profilePath.filePath(keyFileName));
        if (keyFile.open(QFile::WriteOnly))
        {
            keyFile.write(pem);
            keyFile.close();
        }
    }
    return !pkey.isNull();
}

bool SslHelper::RequestCertToFile(QString login, QString password)
{
    if(GenerateCertSignRequest())
    {
        QEventLoop eventLoop;
        QNetworkAccessManager networkAccessManager;
        QUrl url("https://cloud.onmydisk.com/sign.php");
        QNetworkRequest request(url);
        connect(&networkAccessManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));


        QByteArray postData,boundary="1BEF0A57BE110FD467A";

        postData.append("--"+boundary+"\r\n");
        postData.append("Content-Disposition: form-data; name=\"");
        postData.append("name");
        postData.append("\"\r\n\r\n");
        postData.append(login);
        postData.append("\r\n");



        postData.append("--"+boundary+"\r\n");
        postData.append("Content-Disposition: form-data; name=\"");
        postData.append("pass");
        postData.append("\"\r\n\r\n");
        postData.append(password);
        postData.append("\r\n");

        postData.append("--"+boundary+"\r\n");
        postData.append("Content-Disposition: form-data; name=\"");
        postData.append("CSR");
        postData.append("\"\r\n");
        postData.append("Content-Transfer-Encoding: binary\r\n\r\n");
        postData.append(signRequest);
        postData.append("\r\n");
        postData.append("--"+boundary+"--\r\n");

        request.setHeader(QNetworkRequest::ContentTypeHeader,
            "multipart/form-data; boundary="+boundary);
        request.setHeader(QNetworkRequest::ContentLengthHeader,
            QByteArray::number(postData.length()));

        //Wingardium Leviosa!
        QNetworkReply *reply = networkAccessManager.post(request, postData);
        eventLoop.exec(); // blocks stack until "finished()" has been called

        if (reply->error() == QNetworkReply::NoError)
        {
            QByteArray replyData = reply->readAll();
            cert = QSslCertificate(replyData);
            if (cert.isNull())
            {
                delete reply;
                return false;
            }
            else
            {
                QFile certFile(profilePath.filePath(certFileName));
                bool result;
                if (certFile.open(QFile::WriteOnly))
                {
                    certFile.write(replyData);
                    certFile.close();
                    result = true;
                }
                 else
                {
                    result = false;
                    qDebug() << "Cannot write cert file";
                }

                delete reply;
                return result;
            }
        }
        else
        {
            //failure
            qDebug() << "Failure" << reply->errorString();
            delete reply;
            return false;
        }
    }
    return false;
}

bool SslHelper::SaveCaToFile()
{
    QFile EmbeddedCaCert(":/icons/resource/ca.cert");
    if (EmbeddedCaCert.open(QFile::ReadOnly))
    {
        QFile caCertFile(profilePath.filePath(caCertFileName));
        if (caCertFile.open(QFile::WriteOnly))
        {
            caCertFile.write(EmbeddedCaCert.readAll());
            EmbeddedCaCert.close();
            caCertFile.close();
            return true;
        }
        EmbeddedCaCert.close();
    }
    return false;
}

bool SslHelper::privateKeyExists()
{
    return QFile::exists(profilePath.filePath(keyFileName));
}

bool SslHelper::certificateExists()
{
    return QFile::exists(profilePath.filePath(certFileName));
}

bool SslHelper::caCertificateExists()
{
    return QFile::exists(profilePath.filePath(caCertFileName));
}

bool SslHelper::loadPrivateKey()
{
    QFile keyFile(profilePath.filePath(keyFileName));
    if (keyFile.exists())
    {
        if (keyFile.open(QFile::ReadOnly))
        {
            QByteArray rawKey = keyFile.readAll();
            pkey = QSslKey(rawKey, QSsl::Rsa);
            if (!pkey.isNull())
            {
                RSA *rsa;
                pk=EVP_PKEY_new();
                keyFile.close();
                BIO *bio = BIO_new_mem_buf(rawKey.data(), rawKey.count());
                rsa = PEM_read_bio_RSAPrivateKey(bio, 0, 0, 0);
                EVP_PKEY_assign_RSA(pk,rsa);
                BIO_free(bio);
                return true;
            }
        }
    }
    return false;
}

bool SslHelper::loadCertificate()
{
    QFile certFile(profilePath.filePath(certFileName));
    if (certFile.exists())
    {
        if (certFile.open(QFile::ReadOnly))
        {
            cert = QSslCertificate(certFile.readAll());
            return !cert.isNull();
        }
    }
    return false;
}


void SslHelper::reset(QDir profilePath)
{
  //cert.clear();
  QFile::remove(profilePath.filePath(caCertFileName));
  QFile::remove(profilePath.filePath(keyFileName));
  QFile::remove(profilePath.filePath(certFileName));
}

void SslHelper::reset()
{
    //cert.clear();
    QFile::remove(profilePath.filePath(caCertFileName));
    QFile::remove(profilePath.filePath(keyFileName));
    QFile::remove(profilePath.filePath(certFileName));

}



int SslHelper::getSerialNumber(QSslCertificate cert)
{
    int sn = -1;    
    if (!cert.isNull())
    {
        QByteArray raw_sn = cert.serialNumber();
        QByteArray parsed_sn;
        //filter only hex digits
        for (int i = 0; i < raw_sn.size(); i++)
            if (isxdigit(raw_sn[i]))
            {
                parsed_sn.append(raw_sn[i]);
            }
        //convert to int
        bool ok;
        int parsed_int = parsed_sn.toInt(&ok, 16);
        if (ok) sn = parsed_int;
    }
    return sn;
}


QString SslHelper::GetSubjectNameCN()
{
    if (certificateExists())
    {
        if (cert.isNull())
        {
            if (loadCertificate())
            {
                QStringList info = cert.subjectInfo(QSslCertificate::CommonName);
                if (info.count() > 0)
                {
                    return cert.subjectInfo(QSslCertificate::CommonName).at(0);
                }
            }
        }
        else
        {
            QStringList info = cert.subjectInfo(QSslCertificate::CommonName);
            if (info.count() > 0)
            {
                return cert.subjectInfo(QSslCertificate::CommonName).at(0);
            }
        }
    }
    return QString("");
}


bool SslHelper::GenerateCertSignRequest()
{
    X509_REQ *x;
    X509_NAME *name=NULL;

    x=X509_REQ_new();

    X509_REQ_set_pubkey(x,pk);

    name=X509_REQ_get_subject_name(x);

    /* This function creates and adds the entry, working out the
     * correct string type and performing checks on its length.
     * Normally we'd check the return value for errors...
     */
    X509_NAME_add_entry_by_txt(name, "C", MBSTRING_ASC, reinterpret_cast<unsigned char const*>("."), -1, -1, 0);
    X509_NAME_add_entry_by_txt(name,"CN", MBSTRING_ASC, reinterpret_cast<unsigned char const*>(QHostInfo::localHostName().toLatin1().constData()), -1, -1, 0);


    if (X509_REQ_sign(x,pk,EVP_sha1()))
    {
        //save it to file
        BIO *bio = BIO_new(BIO_s_mem());
        PEM_write_bio_X509_REQ(bio, x);
        char *data;
        long size = BIO_get_mem_data(bio, &data);
        signRequest = QByteArray(data, size);
        BIO_free(bio);
        /* for debug only
        QFile csrFile(profilePath.filePath("request.csr"));
        if (csrFile.open(QFile::WriteOnly))
        {
            csrFile.write(signRequest);
            csrFile.close();
        }
        */
        return true;
    }
    else
    {
        return false;
    }
}

void SslHelper::certSigned(QNetworkReply *reply)
{
    QByteArray replyData = reply->readAll();
    cert = QSslCertificate(replyData);
}

