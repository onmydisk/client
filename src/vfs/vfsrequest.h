/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef VFSREQUEST_H
#define VFSREQUEST_H

#include <QString>
#include <common.h>
#include <QDataStream>
#include "qstat.h"

class VFSRequest
{
public:
    VFSRequest();
    VFSRequest(int cmd, QString p, QString np);
    int cmd_type;
    quint64 fh;
    QString path;
    QString newPath;
    QString protocol_version;
    int protocol_stream_version;
    QString client_type;
    QByteArray data;
    VFSFileInfo qStat;
    bool openRO;
    bool openRW;
    bool openWO;
    bool openAppend;
    quint64 size;
    quint64 offset;
    quint64 share_id;
//    int portNumber;


    friend QDataStream & operator<<(QDataStream & ds, const VFSRequest &VFSRequestData);
    friend QDataStream & operator>>(QDataStream & ds, VFSRequest &VFSRequestData);
};

inline QDataStream & operator<<(QDataStream & ds, const VFSRequest &VFSRequestData)
{
//    qDebug() << "cmd_type: " << VFSRequestData.cmd_type;
    ds << VFSRequestData.cmd_type;
    switch (VFSRequestData.cmd_type) {
    case CMD_WRITE:
    {
        ds << VFSRequestData.fh;
        ds << VFSRequestData.size;
        ds << VFSRequestData.offset;
        ds << VFSRequestData.data;
        break;
    }
    case CMD_READ:
    {
        ds << VFSRequestData.fh;
        ds << VFSRequestData.size;
        ds << VFSRequestData.offset;
        break;
    }
    case CMD_MKNODE:
    case CMD_MKDIR:
    {
        ds << VFSRequestData.path;
        ds << VFSRequestData.qStat;
        break;
    }
    case CMD_OPEN:
    {
        ds << VFSRequestData.path;
        ds << VFSRequestData.openRO;
        ds << VFSRequestData.openRW;
        ds << VFSRequestData.openWO;
        ds << VFSRequestData.openAppend;
        break;
    }
    case CMD_RELEASE:
    {
        ds << VFSRequestData.fh;
    }
    case CMD_RENAME:
    {
        ds << VFSRequestData.path;
        ds << VFSRequestData.newPath;
        break;
    }
    case CMD_CONNECT:
    {
        ds << VFSRequestData.protocol_version;
        ds << VFSRequestData.protocol_stream_version;
        ds << VFSRequestData.client_type;
        break;
    }
    case CMD_PING:
    {
        ds << VFSRequestData.share_id;
        break;
    }
    case CMD_RESCAN:
    {
        ds << VFSRequestData.share_id;
        break;
    }
    case CMD_DELETE:
    {
        ds << VFSRequestData.share_id;
        break;
    }
    default:
        ds << VFSRequestData.path;
        break;
    }

    return ds;
}

inline QDataStream & operator>>(QDataStream & ds, VFSRequest &VFSRequestData)
{
    //return if no data
    if (ds.atEnd()) return ds;

    ds >> VFSRequestData.cmd_type;


    switch (VFSRequestData.cmd_type) {
    case CMD_WRITE:
    {
        ds >> VFSRequestData.fh;
        ds >> VFSRequestData.size;
        ds >> VFSRequestData.offset;
        ds >> VFSRequestData.data;
        break;
    }
    case CMD_READ:
    {
        ds >> VFSRequestData.fh;
        ds >> VFSRequestData.size;
        ds >> VFSRequestData.offset;
        break;
    }
    case CMD_MKNODE:
    case CMD_MKDIR:
    {
        ds >> VFSRequestData.path;
        ds >> VFSRequestData.qStat;
        break;
    }
    case CMD_OPEN:
    {
        ds >> VFSRequestData.path;
        ds >> VFSRequestData.openRO;
        ds >> VFSRequestData.openRW;
        ds >> VFSRequestData.openWO;
        ds >> VFSRequestData.openAppend;
        break;
    }
    case CMD_RELEASE:
    {
        ds >> VFSRequestData.fh;
    }
    case CMD_RENAME:
    {
        ds >> VFSRequestData.path;
        ds >> VFSRequestData.newPath;
        break;
    }
    case CMD_CONNECT:
    {
        ds >> VFSRequestData.protocol_version;
        ds >> VFSRequestData.protocol_stream_version;
        ds >> VFSRequestData.client_type;
        break;
    }
    case CMD_PING:
    {
        ds >> VFSRequestData.share_id;
        break;
    }
    case CMD_RESCAN:
    {
        ds >> VFSRequestData.share_id;
        break;
    }
    case CMD_DELETE:
    {
        ds >> VFSRequestData.share_id;
        break;
    }
    default:
        ds >> VFSRequestData.path;
        break;
    }

    return ds;
}

#endif // VFSREQUEST_H
