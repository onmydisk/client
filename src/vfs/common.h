/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef COMMON_H
#define COMMON_H

#include <errno.h>
#include <QByteArray>
#include <QDebug>

#ifdef WIN32
typedef unsigned uid_t;
typedef unsigned gid_t;
#else
#include "fuse.h"
#include <fuse/fuse_lowlevel.h>
struct fuse_server {
    pthread_t pid;
    struct fuse *fuse;
    struct fuse_chan *ch;
    int failed;
    int running;
    char *mountpoint;
    int multithreaded;
    int foreground;
};
#endif

//omd protocol version
#define PROTOCOL_VERSION "0.3"
#ifdef QT5
#define PROTOCOL_STREAM_VERSION 5
#else
#define PROTOCOL_STREAM_VERSION 4
#endif
#define USER_PORT_OFFSET 4000

//cmd codes
#define CMD_PING    0
#define CMD_READDIR 1
#define CMD_GETATTR 2
#define CMD_OPEN    3
#define CMD_RELEASE 4
#define CMD_MKNODE  5
#define CMD_RENAME  6
#define CMD_UNLINK  7
#define CMD_RMDIR   8
#define CMD_READ    9
#define CMD_WRITE   10
#define CMD_MKDIR   11
#define CMD_STATVFS 12

//system commands
#define CMD_CONNECT    100
#define CMD_RESCAN    101
#define CMD_DELETE    102

//error codes
#define ERR_SOCKRO -101
#define ERR_SOCKCLOSED -102
#define ERR_NOTFOUND -103
#define ERR_EXISTS  -104
#define ERR_ACCESS  -105
#define ERR_NOTADIR -106
#define ERR_ENOTEMPTY   -107
#define ERR_CLIENT_OUTDATED   -108
#define ERR_CERT_REMOVED   -109
#define ERR_NO_USER   -110
#define ERR_FUSE_NOT_STARTED   -111
#define ERR_FUSE_NOT_FINISHED   -112

//communication codes
#define OK_RESPONSE 0
#define ERR_RESPONSE -201
#define ERR_TIMEOUT -202

//Timeouts and intervals
#define PING_INTERVAL 20000     //interval between pings
#define PING_TIMEOUT  5000      //if fired, connection is considered broken
//Common response timeout
#define RESPONSE_TIMEOUT 5000
//Connection timeout
#define CONNECT_TIMEOUT 10000
#define RECONNECT_INTERVAL 2000
//Scan timeout
#define SCAN_TIMEOUT 20000



int getSerialNumber(const QByteArray &raw_sn);

#endif // COMMON_H
