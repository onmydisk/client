/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "vfsrequest.h"

VFSRequest::VFSRequest()
{
    cmd_type = -1;
    path = QString("");
    newPath = QString("");
    protocol_version = QString("");
}

VFSRequest::VFSRequest(int cmd, QString p, QString np)
{
    cmd_type = cmd;
    path = p;
    newPath = np;
}
