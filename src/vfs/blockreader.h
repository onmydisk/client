/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef BLOCKREADER_H
#define BLOCKREADER_H

#include <QDataStream>
#include <QBuffer>
#include <QCoreApplication>
#include <QEventLoop>
#include <QDebug>

class BlockReader
{
public:
    BlockReader(QIODevice *io, int v);
    QDataStream& stream();

private:
    // Blocking reads data from socket until buffer size becomes exactly n. No
    // additional data is read from the socket.
    int readMax(QIODevice *io, int n);
    QBuffer buffer;
    QDataStream _stream;
};

#endif // BLOCKREADER_H
