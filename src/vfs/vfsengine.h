/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef VFSENGINE_H
#define VFSENGINE_H

#include <QThread>
#include <QTimer>
#include <QList>
#include <QBuffer>
#include <QDataStream>
#include <QSignalMapper>
#include <QMutex>
#include "common.h"
#include "blockreader.h"
#include "blockwriter.h"
#include "vfsrequest.h"
#include "vfsresponse.h"
#include "sslhelper.h"
#include <QSslSocket>

class VFSEngine : public QThread
{
    Q_OBJECT
public:
    explicit VFSEngine(QString sharePath, QString keyDir, QString clientType, QObject *parent = 0);
    ~VFSEngine();
    void SetupSsl(QList<QSslCertificate> cas, QSslCertificate cert, QSslKey pKey);
    void run();

private:
    bool isConnected;
    bool disconnectRequested;
    bool isReconnecting;

    int connectionCount;

    QString share_path;
    QString client_type;
    QSslSocket socket;
    QList<QSslSocket*> dataSockets;
    QMap<int,  QTimer *> op_timers;

    int version;

    QHash<quint64, QFile *> op_files;
    QSignalMapper mapper;
    QTimer pingTimer;
    SslHelper helper;
    //data request
    quint64 requestSize;
    quint64 bytesRecieved;
    QBuffer buffer;
    QDataStream _stream;
    quint64 blockSize;

    //system response
    quint64 responsetSize;
    quint64 s_bytesRecieved;
    QBuffer s_buffer;
    QDataStream s_stream;
    quint64 s_blockSize;

    uint share_id, deleted_id;

    VFSResponse handleDataRequest(VFSRequest request);
    void connectResponse(int retCode);
    void cleanup(bool reconnect);
    int randInt(int low, int high);
    void doReconnect(bool delayed, bool forced = false);

    void handleReaddir(VFSResponse *response, VFSRequest *request);
    void handleGetattr(VFSRequest *request, VFSResponse *response);
    void handleMkdir(VFSResponse *response, VFSRequest *request);
    void handleMknode(VFSRequest *request, VFSResponse *response);
    void handleOpen(VFSResponse *response, VFSRequest *request);
    void handleRelease(VFSRequest *request, VFSResponse *response);
    void handleUnlink(VFSRequest* request, VFSResponse* response);
    void hanldeRmdir(VFSRequest *request, VFSResponse *response);
    void handleRename(VFSRequest *request, VFSResponse *response);
    void handleRead(VFSRequest* request, VFSResponse* response);
    void handleWrite(VFSRequest* request, VFSResponse* response);
    void handleStatVFS(VFSRequest* request, VFSResponse* response);

    void addOpTimer(QTimer *timer, int op_code);
    void deleteDeleted();

signals:
    void Connected();
    void Disconnected(bool disconnectRequested);
    void ConnectionError(QString err, bool reconnect);
    void CertificateLoaded(QSslCertificate cert);
    void sslErrors(QList<QSslError>);
    bool WantNewCert();
    void TX(qint64 bytesSent);
    void RX(qint64 bytesSent);
    void responseReceived(int op_code, int ret_code);
    void FilesAccessNotification(int op_code, QString fileName);

public slots:
    void Connect();
    void Disconnect();
    void sendRequest(int cmd_type);
    void setSharePath(QString sharePath);
    void setCredentials(QString user, QString password);

private slots:
    void tX(qint64 bytesSent);
    void rX(qint64 bytesSent);
    void responseTimout(int response_type);
    void localShareDisconnected();
    void handleDataRequest();
    void handleSystemResponse();
    void connectionEncrypted();
    void dataConnectionEncrypted();
    void sslError(QList<QSslError> errList);
    void connectionError( QAbstractSocket::SocketError error );
    void doPing();
};

#endif // VFSENGINE_H
