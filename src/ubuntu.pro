QMAKEVERSION = $$[QMAKE_VERSION]
ISQT5 = $$find(QMAKEVERSION, ^[2-9])
isEmpty( ISQT5 ) {
error("Use the qmake include with Qt5.0 or greater");
}

TEMPLATE = subdirs
SUBDIRS  = src
