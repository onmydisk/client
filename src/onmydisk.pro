#-------------------------------------------------
#
# Project created by QtCreator 2013-12-17T09:55:45
#
#-------------------------------------------------


QT       += core gui network
CONFIG   += rtti

greaterThan(QT_MAJOR_VERSION, 4): {
    QT += widgets
    DEFINES += QT5
}


contains(CONFIG, DEPLOY) {
    DEFINES += APP_VERSION=\\\"$$BUILD_VERSION\\\"
}else{
    unix:GET_REVISION =  $$system(git describe --long >revision)
    unix:REVISION   = $$system(cat revision)
    win32:REVISION  = $$system(type revision)
    DEFINES += APP_VERSION=\\\"$$REVISION\\\"
}



CONFIG(release, debug|release):DEFINES += RELEASE

TARGET = onmydisk
TEMPLATE = app


unix:LIBS +=  -ldl -lrt -lcrypto
win32:LIBS += -LC:/OpenSSL-Win32/lib -LC:/OpenSSL-Win32/lib/MinGW -lubsec -llibeay32

INCLUDEPATH +=  foldpanel singleinstance vfs updater
DEPENDPATH +=  foldpanel singleinstance vfs updater
win32:INCLUDEPATH += C:/OpenSSL-Win32/include



unix:DESTDIR = $$PWD/../bin/linux
win32:DESTDIR = $$PWD/../bin/win32

win32:RC_FILE = icon.rc

DEFINES += _FILE_OFFSET_BITS=64 FUSE_USE_VERSION=27

SOURCES += main.cpp \
    maindialog.cpp \
    credentialsdialog.cpp

win32:SOURCES += updater/updatemanager.cpp

HEADERS  += \
    maindialog.h \
    credentialsdialog.h

win32:HEADERS  +=    updater/updatemanager.h

FORMS    += \
    maindialog.ui \
    statusbar.ui \
    credentialsdialog.ui \
    statistics.ui

RESOURCES += \
    icons.qrc


include ($$PWD/singleinstance/qtsingleapplication.pri)
include ($$PWD/vfs/vfs.pri)
include ($$PWD/foldpanel/foldpanel.pri)



app.path = /usr/bin
app.files = $$DESTDIR/onmydisk
icon.path = /usr/share/icons/hicolor/scalable/apps
icon.files = onmydisk.svg
desktopentry.path = /usr/share/applications
desktopentry.files = onmydisk.desktop


unix:INSTALLS += app icon desktopentry

OTHER_FILES += \
    icon.rc \
    onmydisk.spec \
    onmydisk.desktop \
    maketar.sh

