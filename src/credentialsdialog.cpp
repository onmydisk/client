/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "credentialsdialog.h"
#include "ui_credentialsdialog.h"

CredentialsDialog::CredentialsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CredentialsDialog)
{
    ui->setupUi(this);
}

bool CredentialsDialog::getCredentials(QString* userName, QString* password)
{
    ui->nameEdit->setText(*userName);
    ui->passwordEdit->setText(*password);

    if (exec())
    {
       *userName = ui->nameEdit->text();
       *password = ui->passwordEdit->text();
       return true;
    } else
       return false;
}

void CredentialsDialog::resultOk()
{
    setResult(QDialog::Accepted);
}

void CredentialsDialog::resultCancel()
{
    setResult(QDialog::Rejected);
}


CredentialsDialog::~CredentialsDialog()
{
    delete ui;
}
