#!/bin/sh
#Prepare source tarball for building RPM

git archive  --format=tar --prefix=onmydisk-0.3/src/ master | gzip -c > onmydisk-0.3.tar.gz
tar -xvf onmydisk-0.3.tar.gz
cd onmydisk-0.3/src
mv onmydisk.pro src.pro
mv ubuntu.pro ../onmydisk.pro
cd ../../
tar -czf onmydisk_0.3.orig.tar.gz onmydisk-0.3
rm -rf onmydisk-0.3
rm onmydisk-0.3.tar.gz
