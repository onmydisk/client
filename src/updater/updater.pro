#-------------------------------------------------
#
# Project created by QtCreator 2014-05-14T12:30:20
#
#-------------------------------------------------


QT       += core gui network
CONFIG   += rtti

greaterThan(QT_MAJOR_VERSION, 4): {
    QT += widgets
    DEFINES += QT5
}

TARGET = updater
TEMPLATE = app


SOURCES += main.cpp\
        updater.cpp\
        updatemanager.cpp

HEADERS  += updater.h\
            updatemanager.h

FORMS    += \
    updater.ui


DESTDIR = $$PWD/../../bin/win32

RESOURCES += \
    icons.qrc

OTHER_FILES += \
    icon.rc \
    manifest.xml \
    manifest.xml


win32:RC_FILE = icon.rc
