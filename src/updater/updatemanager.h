/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef UPDATEMANAGER_H
#define UPDATEMANAGER_H

#include <QObject>
#include <stdio.h>

#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslError>
#include <QStringList>
#include <QTimer>
#include <QUrl>



QT_BEGIN_NAMESPACE
class QSslError;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class UpdateManager: public QObject
{
    Q_OBJECT
private:
    QNetworkAccessManager manager;
    QUrl client_url;
    QUrl version_url;
    QString local_version, new_version;

public:
    UpdateManager(QObject *parent = 0);
    UpdateManager(const QString &version, QObject *parent = 0);
    void init();
    void doDownload(const QUrl &url);
    bool saveToDisk(const QString &filename, QIODevice *data);
    void update();
    void checkUpdates();
    void cancel();
    QString version() {return new_version;}

public slots:
    void downloadFinished(QNetworkReply *reply);
    void sslErrors(const QList<QSslError> &errors);
signals:
    void checkUpdatesCompleted(bool updatesAvailable);
    void updateCompleted();
    void updateError(const QString &err);

};


#endif // UPDATEMANAGER_H
