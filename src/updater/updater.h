#ifndef UPDATER_H
#define UPDATER_H

#include <QWidget>
#include "updatemanager.h"

namespace Ui {
class Updater;
}

typedef enum {
    U_NONE = 0,
    U_ERROR,
    U_WARNING,
    U_AVAILABLE,
    U_UNAVAILABLE,
    U_CHECKING,
    U_LOADING,
    U_LOADED,

} update_state;

class Updater : public QWidget
{
    Q_OBJECT

public:
    explicit Updater(QWidget *parent = 0);
    ~Updater();

private:
    Ui::Updater *ui;
    update_state m_state;
    bool m_auto_update;
    bool m_started_from_omd;
    bool m_silent;
    bool m_omd_was_runnung;
    int m_animation_step;
    QTimer animationTimer;
    QTimer checkTimer;
    UpdateManager manager;
    void setState(update_state state);
    void setState(update_state state, const QString &message);
    void restoreSettings();
    void saveSettings();
    void init();
    void startAnimation();
    void stopAnimation();
    void error();
    bool checkOmdIsRunning();
    void restartOmdAndExit();

private slots:
    void btnClick();
    void autoCbClick();
    void checkUpdates();
    void downloadUpdates();
    void checkUpdatesCompleted(bool updatesAvailable);
    void updateCompleted();
    void updateError(const QString &err);
    void animate();

};

#endif // UPDATER_H
