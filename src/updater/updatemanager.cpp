/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "updatemanager.h"
#include <QSettings>
#include <QDir>
#include <QDebug>

UpdateManager::UpdateManager(QObject *parent) :
QObject(parent)
{
      QSettings settings("onmydisk", "settings");
      local_version = settings.value("version", "0.3").toString();
      new_version = local_version;
      init();
}


UpdateManager::UpdateManager(const QString &version, QObject *parent) :
QObject(parent),
local_version(version),
new_version(version)
{
    init();
}


void UpdateManager::init()
{
    client_url = "https://onmydisk.com/clients/onmydisk.exe";
    version_url = "https://onmydisk.com/clients/version";
    connect(&manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(downloadFinished(QNetworkReply*)));

}

void UpdateManager::doDownload(const QUrl &url)
{
   QNetworkRequest request(url);
   QNetworkReply *reply = manager.get(request);
   #ifndef QT_NO_SSL
   connect(reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(sslErrors(QList<QSslError>)));
   #endif
}


bool UpdateManager::saveToDisk(const QString &filename, QIODevice *data)
{
      QFile file(filename);
      if (!file.open(QIODevice::WriteOnly))
      {
          qDebug() << "Could not open file for writing" << file.errorString();
          emit updateError(QString(tr("Could not open %1 for writing: %2")).arg(filename, file.errorString()));
          return false;
      }

      file.write(data->readAll());
      file.close();

      local_version = new_version;
      QSettings settings("onmydisk", "settings");
      settings.setValue("version", new_version);
      emit updateCompleted();
      qDebug() << "Client successfully updated";

      return true;
}

void UpdateManager::cancel()
{

}


void UpdateManager::update()
{
      // load client
      doDownload(client_url);

}

void UpdateManager::checkUpdates()
{
     // get version
      doDownload(version_url);
}

void UpdateManager::sslErrors(const QList<QSslError> &sslErrors)
{
  #ifndef QT_NO_SSL
      QString err = "";
      foreach (const QSslError &error, sslErrors)
      {   if (err != "") err += ", ";
          err +=  error.errorString();
      }
      emit updateError(QString(tr("Ssl error: %1")).arg(err));
  #else
      Q_UNUSED(sslErrors);
  #endif
}

void UpdateManager::downloadFinished(QNetworkReply *reply)
{
      QUrl url = reply->url();

      if (reply->error()) {
          qDebug() << "Download of failed: " << url.toString() << reply->errorString();
          emit updateError(QString(tr("Download of %1 failed: %2")).arg(url.toString(), reply->errorString()));
          return;
      }


      if (url == version_url)
      {

          //read version
          QString version(reply->readAll());
          new_version = version.trimmed();
          emit checkUpdatesCompleted(new_version != local_version);


      } else
      if (url == client_url)
      {
              QString filename = QString("/onmydisk.exe").prepend(QDir::currentPath());

              saveToDisk(filename, reply);

      }

      reply->deleteLater();

}

