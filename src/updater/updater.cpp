#include "updater.h"
#include "ui_updater.h"
#include <QSettings>
#include <QDesktopWidget>
#include <QProcess>
#include <QDir>

#include <windows.h>
#include <shellapi.h>

Updater::Updater(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Updater),
    animationTimer(this),
    checkTimer(this),
    manager(this)
{
    ui->setupUi(this);
    m_state = U_NONE;
    m_animation_step = 0;
    m_started_from_omd = (QCoreApplication::arguments().count() > 1 ) ? QCoreApplication::arguments().at(1) == "-autoupdate" : false;

    //window frame with a single close button
    setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint & ~Qt::WindowMinimizeButtonHint);
    setWindowIcon(QIcon(":icons/app.ico"));
    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());
    connect(&manager, SIGNAL(checkUpdatesCompleted(bool)), this, SLOT(checkUpdatesCompleted(bool)));
    connect(&manager, SIGNAL(updateCompleted()), this, SLOT(updateCompleted()));
    connect(&manager, SIGNAL(updateError(QString)), this, SLOT(updateError(QString)));
    connect(&animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
    connect(&checkTimer, SIGNAL(timeout()), this, SLOT(downloadUpdates()));
    connect(ui->exitButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->contimueButton, SIGNAL(clicked()), this, SLOT(btnClick()));
    connect(ui->autoCB, SIGNAL(clicked()), this, SLOT(autoCbClick()));

    m_omd_was_runnung = checkOmdIsRunning();
    restoreSettings();

    m_silent = m_auto_update || m_started_from_omd;

    if (m_silent)
    {
        // if auto update, initiate update procudure
        checkUpdates();
    }
    else
    {
        setState(U_NONE, tr("Welcome to On My Disk update service.\nClick \"Continue\" to check for updates."));
    }
}


void Updater::restoreSettings()
{

    QSettings settings("onmydisk", "settings");
    restoreGeometry(settings.value("updater_geometry").toByteArray());
    m_auto_update = settings.value("auto_update", false).toBool();
    ui->autoCB->setChecked(m_auto_update);
}

bool Updater::checkOmdIsRunning()
{
      QProcess tasklist;
      QString process = QString("onmydisk.exe");
      tasklist.start(
            "tasklist",
            QStringList() << "/NH"
                          << "/FO" << "CSV"
                          << "/FI" << QString("IMAGENAME eq %1").arg(process));
      tasklist.waitForFinished();
      QString output = tasklist.readAllStandardOutput();

      return output.startsWith(QString("\"%1").arg(process));

}


void Updater::autoCbClick()
{
    m_auto_update = ui->autoCB->isChecked();
    m_silent = m_auto_update || m_started_from_omd;
}


void Updater::saveSettings()
{
    QSettings settings("onmydisk", "settings");
    settings.setValue("updater_geometry", saveGeometry());
    settings.setValue("auto_update", m_auto_update);
}

void Updater::init()
{
    animationTimer.stop();
    m_state = U_NONE;
    ui->imgLabel->setPixmap(QPixmap(":/icons/big_icons/app_icon.png"));
}

void Updater::startAnimation()
{
    //set first animation icon here
    ui->contimueButton->setEnabled(false);
    ui->imgLabel->setPixmap(QPixmap(":/icons/update_animation/update_icon_00.png"));
    animationTimer.start(100);

}

void Updater::stopAnimation()
{
    animationTimer.stop();
    ui->contimueButton->setEnabled(true);
}

void Updater::error()
{
  animationTimer.stop();
  ui->imgLabel->setPixmap(QPixmap(":/icons/big_icons/error_icon.png"));
  ui->contimueButton->setEnabled(true);
  m_state = U_ERROR;

}

void Updater::checkUpdates()
{
     ui->msgLabel->setText(tr("Checking for updates..."));
     m_state = U_CHECKING;
     //set first animation icon here
     startAnimation();
     manager.checkUpdates();

}

void Updater::downloadUpdates()
{
    if (! checkOmdIsRunning())
    {
      checkTimer.stop();
      ui->msgLabel->setText( QString(tr("On My Disk version %1 is available.\nLoading updates...")).arg(manager.version()));
      m_state = U_LOADING;
      startAnimation();
      manager.update();
    } else
    {
       checkTimer.start(1000);
       setState(U_WARNING, tr("On My Disk client is still running, please close it before continue.\nClick \"Quit\" in main window or in system tray menu."));
    }
}


void Updater::setState(update_state state)
{
    switch (state)
    {
    case U_NONE:
        init();
        break;
    case U_CHECKING:
        checkUpdates();
        break;
    case U_LOADING:
        downloadUpdates();
        break;
    case U_WARNING:
        ui->imgLabel->setPixmap(QPixmap(":/icons/big_icons/warning_icon.png"));
        stopAnimation();
        m_state = state;
        break;
    case U_AVAILABLE:
        ui->imgLabel->setPixmap(QPixmap(":/icons/big_icons/info_icon.png"));
        stopAnimation();
        ui->contimueButton->setVisible(true);
        ui->contimueButton->setText(tr("Update!"));
        m_state = state;
        break;
    case U_UNAVAILABLE:
        ui->imgLabel->setPixmap(QPixmap(":/icons/big_icons/info_icon.png"));
        stopAnimation();
        ui->contimueButton->setVisible(true);
        ui->contimueButton->setText(tr("Check again"));
        m_state = state;
        break;
    case U_LOADED:
        ui->imgLabel->setPixmap(QPixmap(":/icons/big_icons/info_icon.png"));
        ui->contimueButton->setVisible(false);
        //ui->contimueButton->setText(tr("Start!"));
        stopAnimation();
        m_state = state;
       // m_omd_was_runnung = true;
        break;
    case U_ERROR:
        error();
        break;
    default:
        break;
    }
}

void Updater::setState(update_state state, const QString &message)
{
    ui->msgLabel->setText(message);
    setState(state);
}

void Updater::btnClick()
{

    switch (m_state) {
    case U_UNAVAILABLE:
    case U_WARNING:
    case U_ERROR:
    case U_NONE:
        setState(U_CHECKING);
        break;
    case U_AVAILABLE:
        setState(U_LOADING);
        break;
    case U_LOADING:
        manager.cancel();
        setState(U_ERROR, tr("Update process cancelled."));
        break;
    case U_LOADED:
        restartOmdAndExit();
        break;
    default:
        break;
    }

}

void Updater::animate()
{
    ui->imgLabel->setPixmap(QPixmap(QString(":/icons/update_animation/update_icon_%1.png").arg(m_animation_step, 2, 10, QChar('0'))));
    if (++m_animation_step > 15) m_animation_step = 0;
}

void Updater::checkUpdatesCompleted(bool updatesAvailable)
{
    if (updatesAvailable)
    {
        if (! m_silent)
        {
            setState(U_AVAILABLE, QString(tr("Updated On My Disk version %1 is available for your system.\nClick \"Update\" to proceed with update.")).arg(manager.version()));
        } else
        {
            downloadUpdates();
        }

    } else
    {
        if (! m_started_from_omd)
        {
            setState(U_UNAVAILABLE, tr("Your system is up to date."));
        } else
        {
            restartOmdAndExit();
        }


    }

}

void Updater::updateCompleted()
{
  if (! m_started_from_omd)
  {
        setState( U_LOADED, tr("Update successfully completed.\nStart On My Disk and enjoy!"));
  } else
  {
       restartOmdAndExit();
  }

}


void Updater::updateError(const QString &err)
{

    setState( U_ERROR, err);
}




void Updater::restartOmdAndExit()
{
    stopAnimation();
    setState( U_LOADED, tr("Restarting On My Disk"));
    m_omd_was_runnung = true;
    close();

}


Updater::~Updater()
{
    saveSettings();
    if (m_omd_was_runnung || m_started_from_omd)
    {

        // start updater process;
       QString cmd = QString("/onmydisk.exe").prepend(QDir::currentPath());
       QString param = QString("-connect");
       // Restarting On My Disk.
       //TODO: potentially dirty thing is here. run elevated onmydisk process when it is not
       //actually necessary just to avoid ACHTUNG message from UAC.
       //Change the verb to "open" when we get the certificate to sign our exe
       ShellExecuteA(0, "runas", cmd.toUtf8().constData(), param.toUtf8().constData(), 0, SW_SHOWNORMAL);

    }

    delete ui;
}
