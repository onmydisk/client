/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDebug>
#include <QHostInfo>
#ifdef QT5
#include <QStandardPaths>
#else
#include <QDesktopServices>
#endif
#include <QWidget>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QStyleFactory>
#include <QMessageBox>
#include <QTimer>
#include <QProcess>
#include <QClipboard>
#include <QImage>
#include <QSslCertificate>
#include "credentialsdialog.h"
#include "ui_statistics.h"
#include "ui_statusbar.h"
#include "ui_maindialog.h"
#include "ui_statusbar.h"
#include "common.h"
#include "blockreader.h"
#include "blockwriter.h"
#include "vfsrequest.h"
#include "vfsresponse.h"
#include "vfsengine.h"
#include "updatemanager.h"
#include "foldpanel.h"
#ifdef WIN32
#include <windows.h>
#else
#include "sys/statvfs.h"
#endif

typedef enum {
 CS_Disconnected = 0,
 CS_Connecting,
 CS_Scanning,
 CS_Connected
}ConnectionState;




class MainDialog : public QWidget
{
    Q_OBJECT

public:
    explicit MainDialog(QWidget *parent = 0);
    void saveSettings();
    void restoreSettings();
    ~MainDialog();
public slots:
    void ipcMessage(const QString &str);
    
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private:
    //GUI

    QWidget * statisticsForm;
    QWidget * statusbarForm;

    Ui::MainDialog * ui;
    Ui::StatisticsForm *ui_statisticsForm;
    Ui::StatusbarForm *ui_statusbarForm;


    FoldablePanel *actionsPanel;
    FoldablePanel *statisticsPanel;
    FoldablePanel *lastClosed, *lastOpen;

    QString keyDir;
    //version
    QString version_short, version_long;

   //Connection data
    ConnectionState m_state;
    QString login, password;

    quint16 share_id;
    QString share_path;
    QString user_key;
    QString m_status;
    bool connectOnStart;
    bool newCertRequested;

    //updates
    #if WIN32
    bool updatesAvailable;
    bool checkUpdatesRequested;
    UpdateManager *uManager;
    bool auto_update;
    QTimer updatesTimer;
    #endif

    //global dirs
    QString homeDir;
    QString appDataDir;
    QMutex mutex;


    //Tray icon
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

    //GUI
    void createTrayIcon();
    void setStatus(const QString &status);
    void setState( ConnectionState state);
    void setRxTx(const QString &tx, const QString &rx);
    //connection
    void connectCloud();
    void disconnectCloud();
    //local share
    void connectLocalShare();
    void disconnectLocalShare();

    void scanFinished(int ret_code);
    void fileOperationCompleted();

    bool getCredentials(QString *login, QString *password);
#if WIN32
    void updateClient(const QString &msg);
#endif
    //VFS
    VFSEngine* engine;

    qint64 totalTX, totalRX;
    QTimer RxTxTimer;
    QTimer animationTimer;
    QTimer accessAnimationTimer;
    uint animation_step;

signals:
    void ConnectCloud();
    void DisconnectCloud();
    void SendRequest(int cmd_type);
    void SetSharePath(QString share_path);
    void SetCredentials(QString user, QString password);


private slots:
    //GUI

    void acquireCertificate();
    void certificateLoaded(QSslCertificate cert);
    void RxTxTimerTick();
    void connectAction();
    void activateAction();
    void panelAnimationStarted(bool &fold);
    void panelAnimationCompleted(bool &fold);
    void selectDirectory();
    void sysTrayActivated(QSystemTrayIcon::ActivationReason reason);
    void TX(qint64 bytesSent);
    void RX(qint64 bytesSent);
    void doConnect();
    void doDisconnect(QString status, bool reconnect);
    //connection handling
    void localShareConnected();
    void localShareDisconnected(bool reconnect);
    void rescanShare();
    void openFiles();
    void openSettings();
    void setConnectOnStart();
    void getNewCertificate();
    void connectionError(QString Err, bool reconnect);
    void responseReceived(int op_code, int ret_code);
    void updateAnimatedIcons();
    void fileAccessAnimation(int op_code, QString fileName);
    void updateFileAcessIcons();
#if WIN32
    void checkUpdatesCompleted(bool updatesAvailable);
    void updateError(const QString &err);
    void checkUpdates();
    void autoUpdatesCheck();
#endif
    //exit
    void appQuit();

};

#endif // MAINDIALOG_H
