/******************************************************************************
**
**      Copyright 2014 On My Disk Team
**
**     Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
**
*******************************************************************************/

#include "maindialog.h"
#include <QToolTip>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QStatusTipEvent>

#if WIN32
#include <windows.h>
#include <shellapi.h>
#endif

MainDialog::MainDialog(QWidget *parent) :
    QWidget(parent),
    statisticsForm(new QWidget(this)),

    ui(new Ui::MainDialog),
    ui_statisticsForm(new Ui::StatisticsForm),
    ui_statusbarForm(new Ui::StatusbarForm),
    #if WIN32
    updatesTimer(this),
    #endif
    RxTxTimer(this),
    animationTimer(this),
    accessAnimationTimer(this)

{

    qRegisterMetaType<QSslCertificate>("QSslCertificate");

    ui->setupUi(this);
    ui_statisticsForm->setupUi(statisticsForm);

    //window frame with a single close button
    setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint & ~Qt::WindowMinimizeButtonHint);
    #if WIN32
    setWindowIcon(QIcon(":icons/onmydisk.svg"));
    #endif
    qDebug() << APP_VERSION;
   // APP_VERSION;
    version_long = APP_VERSION;
    QStringList parsed=version_long.split("-");
    if (parsed.size() > 2 )
        version_short = parsed[0] + "-" + parsed[1];
    else
        version_short = version_long;

    qDebug() << version_short;
    setWindowTitle(QString(tr("On My Disk Client %1")).arg(version_short));
    //updates
    #if WIN32
    updatesAvailable = false;
    checkUpdatesRequested = false;
    uManager = new UpdateManager(version_short, this);
    connect(uManager, SIGNAL(checkUpdatesCompleted(bool)), this, SLOT(checkUpdatesCompleted(bool)));
    connect(uManager, SIGNAL(updateError(QString)), this, SLOT(updateError(QString)));
    connect(ui->actionUpdate, SIGNAL(triggered()), this, SLOT(checkUpdates()));
    connect(&updatesTimer, SIGNAL(timeout()), this, SLOT(checkUpdates()));
    connect(ui->actionAutoUpdate, SIGNAL(triggered()), this, SLOT(autoUpdatesCheck()));
    //one hour interval for checking updates
    updatesTimer.start(3600000);
    #else
    QString desktop = qgetenv("XDG_CURRENT_DESKTOP");
    if (desktop.toLower() != "unity") // do not create tray icon for Unity!!!
    #endif
    //tray icon
    createTrayIcon();

    // hook up all events
    qApp->installEventFilter(this);

    //Data
    m_state  = CS_Disconnected;
    share_id = 0;
    totalTX = 0;
    totalRX = 0;
    newCertRequested = false;
    animation_step = 1;

    //GUI - Vertical layout
    QVBoxLayout *vbl = new QVBoxLayout(this);
#ifdef WIN32
    vbl->setMargin(4);
    vbl->setSpacing(4);
#else
    vbl->setMargin(8);
    vbl->setSpacing(8);
#endif
    vbl->setSpacing(4);
    this->setLayout(vbl);

    // Upper toolbar
    QWidget *tbar = new QWidget(this);
    tbar->setStyleSheet("background-color: #1d2d42;"
                        "background-image: url(:icons/resource/logo-wide.png);"
                        "background-repeat: no;"
                        "background-origin: content;"
                        "padding: 8px;");
    tbar->setMinimumHeight(50);
    vbl->addWidget(tbar);

    FoldablePanel::overhead = tbar->height();

    //GUI actions


    ui->actionConnect->setText(tr("Connect"));

    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(appQuit()) );
    connect(ui->actionConnect, SIGNAL(triggered()), this, SLOT(connectAction()));
    connect(ui->actionActivate, SIGNAL(triggered()), this, SLOT(activateAction()));
    connect(ui->actionConnectOnStart, SIGNAL(triggered()), this, SLOT(setConnectOnStart()));
    connect(ui->actionNewCertificate, SIGNAL(triggered()), this, SLOT(getNewCertificate()));
    connect(ui->actionRescan, SIGNAL(triggered()), this, SLOT(rescanShare()));
    connect(ui->actionFiles, SIGNAL(triggered()), this, SLOT(openFiles()));
    connect(ui->actionSettings, SIGNAL(triggered()), this, SLOT(openSettings()));
    connect(ui->actionBrowse, SIGNAL(triggered()), this, SLOT(selectDirectory()));

    //Actions
    actionsPanel = new FoldablePanel(tr("Actions"), this);
    actionsPanel->addAction(ui->actionConnect);
    actionsPanel->addAction(ui->actionBrowse);
    actionsPanel->addAction(ui->actionConnectOnStart);
    actionsPanel->addAction(ui->actionFiles);
    actionsPanel->addAction(ui->actionSettings);
    actionsPanel->addAction(ui->actionRescan);
    actionsPanel->addAction(ui->actionNewCertificate);
    actionsPanel->addAction(ui->actionQuit);
    vbl->addWidget(actionsPanel);

    FoldablePanel::overhead += actionsPanel->headerHeight();

    //Statistics
    statisticsPanel =  new FoldablePanel(tr("Statistics"), this, true);
    statisticsPanel->addWidget(statisticsForm);
    vbl->addWidget(statisticsPanel);

    FoldablePanel::overhead += statisticsPanel->headerHeight();


    lastOpen = actionsPanel;
    lastClosed = statisticsPanel;

    //stretch
    vbl->addStretch();

    //status bar

     statusbarForm = new QWidget(this);
     ui_statusbarForm->setupUi(statusbarForm);
     vbl->addWidget(statusbarForm);

     FoldablePanel::overhead += statusbarForm->height();
     //correct for margins and spacings
     FoldablePanel::overhead -=  vbl->margin();


     //default locations

#ifdef QT5
     homeDir = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
     appDataDir = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
#else
     homeDir = QDesktopServices::storageLocation(QDesktopServices::HomeLocation);
     appDataDir = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif

    // defalut share path
    share_path = homeDir + QString(tr("/Shared"));


    keyDir = appDataDir;

   // Keyfile stuff
   // Check out/create key dir
    QDir kDir(keyDir);
    if (!kDir.exists()) kDir.mkpath(keyDir);


    //group expanding event
    connect(actionsPanel, SIGNAL(animationStarted(bool &)), this, SLOT(panelAnimationStarted(bool &)));
    connect(statisticsPanel, SIGNAL(animationStarted(bool &)), this, SLOT(panelAnimationStarted(bool &)));
    connect(actionsPanel, SIGNAL(animationFinished(bool &)), this, SLOT(panelAnimationCompleted(bool &)));
    connect(statisticsPanel, SIGNAL(animationFinished(bool &)), this, SLOT(panelAnimationCompleted(bool &)));

    //transfer timer
    connect(&RxTxTimer, SIGNAL(timeout()), this, SLOT(RxTxTimerTick()));
    //animated icons timers
    connect(&animationTimer,SIGNAL(timeout()), this, SLOT(updateAnimatedIcons()));
    connect(&accessAnimationTimer, SIGNAL(timeout()), this, SLOT(updateFileAcessIcons()));


    //Restore settings
    restoreSettings();



    //socket events moved here:
    engine = new VFSEngine(share_path, keyDir, "desktop");
    connect(engine, SIGNAL(CertificateLoaded(QSslCertificate)), this, SLOT(certificateLoaded(QSslCertificate)));
    connect(engine, SIGNAL(Connected()), this, SLOT(localShareConnected()));
    connect(engine, SIGNAL(Disconnected(bool)), this, SLOT(localShareDisconnected(bool)));
    connect(engine, SIGNAL(ConnectionError(QString,bool)), this, SLOT(connectionError(QString,bool)));
    connect(engine, SIGNAL(WantNewCert()),this, SLOT(acquireCertificate()));
    connect(engine, SIGNAL(responseReceived(int,int)), this, SLOT(responseReceived(int,int)));
    connect(engine, SIGNAL(RX(qint64)), this, SLOT(RX(qint64)));
    connect(engine, SIGNAL(TX(qint64)), this, SLOT(TX(qint64)));
    connect(engine, SIGNAL(FilesAccessNotification(int,QString)), this, SLOT(fileAccessAnimation(int,QString)));
    connect(this, SIGNAL(ConnectCloud()), engine, SLOT(Connect()));
    connect(this, SIGNAL(DisconnectCloud()), engine, SLOT(Disconnect()));
    connect(this, SIGNAL(SendRequest(int)), engine, SLOT(sendRequest(int)));
    connect(this, SIGNAL(SetCredentials(QString,QString)), engine, SLOT(setCredentials(QString,QString)));
    connect(this, SIGNAL(SetSharePath(QString)), engine, SLOT(setSharePath(QString)));
    engine->start();

    ui_statusbarForm->imgLabel->setPixmap(QPixmap(":/icons/resource/states/icon_disconnected.png"));
    setStatus(tr("Welcome to On My Disk!"));

    //autoconnect if enabled of after update
    bool doConnect = (QCoreApplication::arguments().count() > 1 ) ? QCoreApplication::arguments().at(1) == "-connect" : connectOnStart;
    if (doConnect) connectCloud();

}

void MainDialog::setState(ConnectionState state)
{

 if (m_state != state)
 {
    QIcon icon;

    switch (state) {
    case CS_Disconnected:
    {
        icon = QIcon(":/icons/resource/states/icon_disconnected.png");
        ui_statusbarForm->imgLabel->setPixmap(QPixmap(":/icons/resource/states/icon_disconnected.png"));
        animationTimer.stop();
        break;
    }
    case CS_Connected:
    {
        icon = QIcon(":/icons/resource/states/icon_online.png");
        ui_statusbarForm->imgLabel->setPixmap(QPixmap(":/icons/resource/states/icon_online.png"));
        animationTimer.stop();
        break;
    }
    case CS_Scanning:
    case CS_Connecting:
    {
        animation_step = 1;
        ui_statusbarForm->imgLabel->setPixmap(QPixmap(":/icons/resource/states/icon_wait_01.png"));
        icon = QIcon(":/icons/resource/states/icon_wait_01.png");
        animationTimer.start(300);
    }
    default:
        break;
    }

    #if WIN32
    trayIcon->setIcon(icon);
    #endif
    ui->actionActivate->setIcon(icon);

    m_state = state;
 }
}


void MainDialog::updateAnimatedIcons()
{
    if (++animation_step > 4) animation_step = 1;
    QIcon icon(QString(":/icons/resource/states/icon_wait_0%1.png").arg(QString::number(animation_step)));
    ui_statusbarForm->imgLabel->setPixmap(QPixmap(QString(":/icons/resource/states/icon_wait_0%1.png").arg(QString::number(animation_step))));
    #if WIN32
    trayIcon->setIcon(icon);
    #endif
    ui->actionActivate->setIcon(icon);
}


void MainDialog::fileOperationCompleted()
{
    QIcon icon(":/icons/resource/states/icon_online.png");
    ui_statusbarForm->imgLabel->setPixmap(QPixmap(":/icons/resource/states/icon_online.png"));
    accessAnimationTimer.stop();
    #if WIN32
    trayIcon->setIcon(icon);
    #endif
    ui->actionActivate->setIcon(icon);
    setStatus("Completed.");
}

void MainDialog::fileAccessAnimation(int op_code, QString fileName)
{
  QString status;
  QFile f(fileName);
  QFileInfo fileInfo(f.fileName());
  QString fname(fileInfo.fileName());
  switch (op_code)
  {
   // case CMD_GETATTR:
    case CMD_READ:
    {
      status = QString ("Reading %1").arg(fname);
      if (! accessAnimationTimer.isActive())
      {
          accessAnimationTimer.start(200);
      }
      setStatus(status);
      break;
    }
    case CMD_WRITE:
    {
      status = QString ("Writing %1").arg(fname);
      if (! accessAnimationTimer.isActive())
      {
          accessAnimationTimer.start(200);
      }
      setStatus(status);
      break;
    }
    case CMD_RELEASE:
    {
       fileOperationCompleted();
       break;
    }

   default: break;

  }
}

void MainDialog::updateFileAcessIcons()
{
    if (++animation_step > 2) animation_step = 1;
    QIcon icon(QString(":/icons/resource/states/icon_transfer_0%1.png").arg(QString::number(animation_step)));
    ui_statusbarForm->imgLabel->setPixmap(QPixmap(QString(":/icons/resource/states/icon_transfer_0%1.png").arg(QString::number(animation_step))));
    #if WIN32
    trayIcon->setIcon(icon);
    #endif
    ui->actionActivate->setIcon(icon);
}


void MainDialog::certificateLoaded(QSslCertificate cert)
{    
    share_id = SslHelper::getSerialNumber(cert);
    QDateTime validTo = cert.expiryDate();
    QString certInfo = QString(tr("Issued to (hostname): %1\nSerial number: %2\nValid to: %3")).arg(cert.subjectInfo(QSslCertificate::CommonName).at(0), QString::number(share_id), validTo.toString("dd.MM.yyyy"));
    ui_statisticsForm->textEdit->setText(certInfo);
}


bool MainDialog::getCredentials(QString* login, QString* password)
{
    CredentialsDialog* cd = new CredentialsDialog(this);
    bool result = cd->getCredentials(login, password);
    delete cd;
    return result;
}


void MainDialog::rescanShare()
{

  int r = QMessageBox::question(this, tr("On My Disk"),
                                      tr("Scanning may take a few minutes.\nDo you want to rescan your files?"),
                                      QMessageBox::Yes | QMessageBox::No);
  if (r == QMessageBox::Yes )
  {
      qDebug() << "Scanning...";
      setStatus(tr("Scanning..."));

      ui->actionConnect->setEnabled(false);
      ui->actionRescan->setEnabled(false);
      setState(CS_Scanning);
      emit SendRequest(CMD_RESCAN);
  }
}


void MainDialog::scanFinished(int ret_code)
{
 switch(ret_code)
 {
    case OK_RESPONSE:
    {
        qDebug() << "Rescanned OK.";
        setStatus(tr("Successfully rescanned."));
        break;
    }
    case ERR_TIMEOUT:
    {
        qDebug() << "Scan timeout.";
        setStatus(tr("Scanning is still in progress, don't go away."));
        break;
    }
    default:
    {
         qDebug() << "Scan error.";
         setStatus(tr("Scan error."));

    }
 }
 ui->actionConnect->setEnabled(true);
 ui->actionRescan->setEnabled(true);
 setState(CS_Connected);

}



// system commands' responses
void MainDialog::responseReceived(int op_code, int ret_code)
{
    switch (op_code)
    {
        case CMD_RESCAN:
        {
            scanFinished(ret_code);
            break;
        }

        #if WIN32
        case CMD_CONNECT:
        {
            if (ret_code == ERR_CLIENT_OUTDATED)
            {
                updateClient(QString(tr("Your On My Disk  client version %1 is outdated.\nDo you want to check for updates?")).arg(version_short));
            }
        }
        #endif

        default:
        {
            break;
        }
    }
    qDebug() << QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss:zzz") << "Response:" << op_code << "code:" << ret_code;

}



void MainDialog::ipcMessage(const QString &str)
{
    qDebug() << str;
    activateAction();
}


void MainDialog::getNewCertificate()
{
    int r = QMessageBox::question(this, tr("On My Disk"),
                                  tr("Do you want to acquire a new certificate?"),
                                    QMessageBox::Yes | QMessageBox::No);

    if (r == QMessageBox::Yes)
    {
        //remove old cert files
        SslHelper::reset(keyDir);
        //Delete old certificate from the database
        newCertRequested = true;
        emit SendRequest(CMD_DELETE);
        if (m_state != CS_Connected)
        {
            emit ConnectCloud();
        }

    }

}

void MainDialog::appQuit()
{

    engine->quit();
    if (!engine->wait(5000))
    {
        engine->terminate();
    }

    saveSettings();
    qApp->quit();

}



void MainDialog::setStatus(const QString &status)
{
    m_status = status;
    ui_statusbarForm->statusLabel->setText(status);
}

void MainDialog::setRxTx(const QString &tx, const QString &rx)
{
    ui_statisticsForm->uploadRate->setText(tx);
    ui_statisticsForm->dowloadRate->setText(rx);
}


//Connect-disconnect
void MainDialog::connectAction()
{

    switch (m_state)
    {
        case CS_Connected:
        {
            disconnectCloud();
            break;
        }
        case  CS_Connecting:
        {
            //stop connecting
             disconnectCloud();
             // call disconnected event manually
             localShareDisconnected(true);
             break;
        }

        case CS_Disconnected:
        {
            connectCloud();
            break;
        }
    default:
        {
            break;
        }
    }
 }


void MainDialog::activateAction()
{
    setVisible(true);
    activateWindow();
}


void MainDialog::saveSettings()
{

    QSettings settings("onmydisk", "settings");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("share_path", share_path);
    settings.setValue("version", version_short
                      );
#if WIN32
    settings.setValue("auto_update", auto_update);
#endif
    settings.setValue("connect_on_start", connectOnStart);
}

void MainDialog::setConnectOnStart()
{
     connectOnStart = ui->actionConnectOnStart->isChecked();
}

void MainDialog::restoreSettings()
{

    QSettings settings("onmydisk", "settings");
    restoreGeometry(settings.value("geometry").toByteArray());
    share_path = settings.value("share_path", share_path).toString();
    connectOnStart = settings.value("connect_on_start", false ).toBool();
    ui->actionConnectOnStart->setChecked(connectOnStart);
#if WIN32
    auto_update = settings.value("auto_update", false).toBool();
    ui->actionAutoUpdate->setChecked(auto_update);
#endif
    ui->actionBrowse->setStatusTip(QString(tr("Shared folder: %1")).arg(share_path));

    //set version ASAP!
    settings.setValue("version", version_short);

}


void MainDialog::panelAnimationStarted(bool &fold)
{
   FoldablePanel* panel = (FoldablePanel *) sender();
   if (!fold)
   {
        if (panel == statisticsPanel && !actionsPanel->isFolded())
        {
            actionsPanel->fold();
            fold = !fold;
        } else
        if (panel == actionsPanel && !statisticsPanel->isFolded())
        {
            statisticsPanel->fold();
            fold = !fold;
        }
   }
}


void MainDialog::panelAnimationCompleted(bool &fold)
{
    FoldablePanel* panel = (FoldablePanel *) sender();
    if (fold)
    {
        if (lastOpen == panel && lastClosed != lastOpen)
        {
           lastClosed->unfold();
        }
        lastClosed = panel;
    } else
    {
        if (lastOpen != panel)
        {
           lastOpen->fold();
        }
        lastOpen = panel;
    }

}


//called from enginge if new certificate needed
void MainDialog::acquireCertificate()
{


    // show dialog if we didn't request  manually
    int r = newCertRequested ? QMessageBox::Yes : QMessageBox::question(this, tr("On My Disk"),
                                                                        tr("Your certificate is expired or does not exist.\nDo you want to acquire a new one?"),

                                                                        QMessageBox::Yes | QMessageBox::Cancel);
    if (r == QMessageBox::Yes)
    {
        //start showing connecting state to avoid clicks on "Connect"
        setState(CS_Connecting);
        setStatus(tr("Login"));



        if (getCredentials(&login, &password))
        {
            newCertRequested = true; // to avoid message on invalid user
            setStatus(tr("Generating new certificate..."));
            emit SetCredentials(login, password);
            return;
        } else
        {
            newCertRequested = false;
            disconnectCloud();
        }
    } else
        disconnectCloud();

}


//Connection management

void MainDialog::connectCloud()
{

    setState(CS_Connecting);
    setStatus(tr("Connecting..."));
    ui->actionBrowse->setEnabled(false);
    ui->actionConnect->setText(tr("Stop connecting"));
    ui->actionConnect->setToolTip(tr("Stop connecting"));
    ui->actionConnect->setStatusTip(tr("Stop connecting"));
    ui->actionConnect->setIcon(QIcon(":/icons/resource/power-off.png"));
    connectLocalShare();

}

void MainDialog::disconnectCloud()
{
    emit DisconnectCloud();
}




// Prepare and mount share
void MainDialog::connectLocalShare()
{
    //  check if share_path exists and create it if user agrees,
    QDir dir(share_path);
    if (!dir.exists())
    {
        int r = QMessageBox::question(this, tr("On My Disk"),
                                      tr("Shared directory '%1'\ndoes not exist yet.\n"
                                         "Do you want to create the directory?").arg(share_path),
                                        QMessageBox::Yes | QMessageBox::Cancel);
        if (r == QMessageBox::Yes)
        {
            dir.mkpath(share_path);
        } else
        {
            localShareDisconnected(true);
            return;
        }
    }


    doConnect();
}




void MainDialog::doConnect()
{
    emit ConnectCloud();
}


void MainDialog::doDisconnect(QString status, bool reconnect)
{
    RxTxTimer.stop();


    setRxTx("","");

    ui->actionConnect->setEnabled(true);
    setStatus(status);
    accessAnimationTimer.stop();

     if (reconnect)
     {
            setState(CS_Connecting);
            qDebug() << "Reconnecting....";
            ui->actionConnect->setText(tr("Stop connecting"));
            ui->actionConnect->setToolTip(tr("Stop connecting"));
            ui->actionConnect->setStatusTip(tr("Stop connecting"));
            ui->actionConnect->setIcon(QIcon(":/icons/resource/power-off.png"));
      } else
      {
            setState(CS_Disconnected);
            ui->actionRescan->setEnabled(false);
            ui->actionBrowse->setEnabled(true);
            ui->actionConnect->setText(tr("Connect"));
            ui->actionConnect->setToolTip(tr("Connect"));
            ui->actionConnect->setStatusTip(tr("Connect to On My Disk cloud"));
            ui->actionConnect->setIcon(QIcon(":/icons/resource/cloud-upload-01.png"));
            qDebug() << "Disconnected.";
      }

}


//Connection is established
void MainDialog::localShareConnected()
{
    RxTxTimer.start(1000);


    ui->actionConnect->setText(tr("Disconnect"));
    ui->actionConnect->setToolTip(tr("Disconnect"));
    ui->actionConnect->setStatusTip(tr("Disconnect from cloud"));
    ui->actionConnect->setIcon(QIcon(":/icons/resource/power-off.png"));
    ui->actionRescan->setEnabled(true);
    ui->actionBrowse->setEnabled(false);

    newCertRequested = false;
    qDebug() << "Connected.";
    setState(CS_Connected);
    setStatus(tr("Connected. Click <a href=\"%1\">here</a> to view your files.").arg("https://cloud.onmydisk.com/"));


    if (statisticsPanel->isFolded())
    {
       statisticsPanel->unfold();
    }
}



//Disconnected
void MainDialog::localShareDisconnected(bool reconnect)
{
 QString status =  reconnect ? tr("Reconnecting..."): tr("Disconnected.");
 doDisconnect(status, reconnect);
}

void MainDialog::connectionError(QString Err, bool reconnect)
{
    if (!reconnect) newCertRequested = false;
    doDisconnect(Err, reconnect);
}


void MainDialog::selectDirectory()
{

     QString dir = QFileDialog::getExistingDirectory(NULL, tr("Select shared directory:"), share_path);
     if (dir != "")
     {
            share_path = dir;
            emit SetSharePath(share_path);
            ui->actionBrowse->setStatusTip(QString(tr("Shared folder: %1")).arg(share_path));
            saveSettings();
     }
     activateAction();
 }

 void MainDialog::TX(qint64 bytesSent)
 {
     totalTX+=bytesSent;
 }

 void MainDialog::RX(qint64 bytesSent)
 {
     totalRX+=bytesSent;
 }



void MainDialog::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(ui->actionActivate);
    trayIconMenu->addAction(ui->actionConnect);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(ui->actionConnectOnStart);
    trayIconMenu->addAction(ui->actionFiles);
    trayIconMenu->addAction(ui->actionSettings);
    //trayIconMenu->addAction(ui->actionNewCertificate);
    trayIconMenu->addSeparator();
    #ifdef WIN32
    trayIconMenu->addAction(ui->actionUpdate);
    trayIconMenu->addAction(ui->actionAutoUpdate);
    trayIconMenu->addSeparator();
    #endif
    trayIconMenu->addAction(ui->actionQuit);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(sysTrayActivated(QSystemTrayIcon::ActivationReason)));


#ifdef WIN32
    trayIcon->setIcon(QIcon(":icons/resource/app_16.png"));
#else
    trayIcon->setIcon(QIcon(":icons/resource/app_128.png"));
#endif
    trayIcon->setToolTip(tr("On My Disk Client"));
    trayIcon->show();
}

void MainDialog::sysTrayActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Trigger || reason == QSystemTrayIcon::DoubleClick)
    {
        activateAction();
    }
}

void MainDialog::openFiles()
{
 QDesktopServices::openUrl(QUrl("https://cloud.onmydisk.com/", QUrl::TolerantMode));
}

void MainDialog::openSettings()
{
 QDesktopServices::openUrl(QUrl("https://cloud.onmydisk.com/index.php/settings/personal", QUrl::TolerantMode));
}






#define MB 1048576
#define KB 1024

void MainDialog::RxTxTimerTick()
{
    QString rxLabel, txLabel;
    double d;
    if (totalTX > MB ) {
        d = (double) totalTX / MB;
        txLabel = QString("Upload: %1 MB/s").arg( QString::number(d, 'f', 2 ));
    }else
    if (totalTX > KB ){
        d = (double) totalTX / KB;
        txLabel = QString("Upload: %1 KB/s").arg( QString::number(d, 'f', 2 ));
    }else
        txLabel = QString("Upload: %1 bytes/s").arg(totalTX);
    if (totalRX > MB ){
        d = (double) totalRX / MB;
        rxLabel = QString("Download: %1 MB/s").arg( QString::number(d, 'f', 2));
    }else
    if (totalRX > KB ){
        d = (double) totalRX / KB;
        rxLabel = QString("Download: %1 KB/s").arg( QString::number(d, 'f', 2));
    }else
        rxLabel = QString("Download: %1 bytes/s").arg(totalRX);

    totalTX = 0;
    totalRX = 0;
    setRxTx(txLabel, rxLabel);
}


bool MainDialog::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::StatusTip)
    {
        QStatusTipEvent *statusEvent = static_cast<QStatusTipEvent *>(event);
        QString statusText = statusEvent->tip();
        if (statusText != "")
            ui_statusbarForm->statusLabel->setText(statusText);
        else
            ui_statusbarForm->statusLabel->setText(m_status);
        event->accept();
        return true;
    } else
    if (event->type() == QEvent::ToolTip && obj != trayIcon)
    {
        event->ignore();
        return true;
    } else
        return QWidget::eventFilter(obj, event);
}

#if WIN32

void MainDialog::autoUpdatesCheck()
{
    auto_update = ui->actionAutoUpdate->isChecked();
}


void MainDialog::checkUpdates()
{
    checkUpdatesRequested = (sender() == ui->actionUpdate);
    qDebug() << "Checking for updates...";
    if (! updatesAvailable)
    {
        uManager->checkUpdates();
    } else
    {
        // start updater;
        updateClient(QString(tr("Update to version %1?")).arg(uManager->version()));
    }
}

void MainDialog::checkUpdatesCompleted(bool updatesAvailable)
{

    this->updatesAvailable = updatesAvailable;
    if ( this->updatesAvailable)
    {
        updatesTimer.stop();
        setStatus(QString(tr("Version %1 is available.")).arg(uManager->version()));
        ui->actionUpdate->setText(tr("Update client"));
        updateClient(QString(tr("Updated client (version %1) is available.\nDo you want to proceed with update?")).arg(uManager->version()));
    } else
    {
        QString msg = QString(tr("Version %1 is up to date.")).arg(version_short);
        if (checkUpdatesRequested)
          QMessageBox::information(this, tr("On My Disk updates"), msg);
        else
          setStatus(msg);
    }
    checkUpdatesRequested = false;

}

void MainDialog::updateClient(const QString &msg)
{
    int r = (auto_update) ? QMessageBox::Yes :  QMessageBox::question(this, tr("On My Disk"), msg,
                                    QMessageBox::Yes | QMessageBox::No);

    if (r ==  QMessageBox::Yes)
    {

        // start updater process;
       QString cmd = QString("/updater.exe").prepend(QDir::currentPath());
       QString arg = QString("-autoupdate");
       bool quit = true;
       // Requesting elevated start of updater
       int result = (int)::ShellExecuteA(0, "runas", cmd.toUtf8().constData(), arg.toUtf8().constData(), 0, SW_SHOWNORMAL);

       if (result <= 32)
       {
                // error handling
          quit = QMessageBox::question(this, tr("On My Disk"), tr("Can not start updater automatically.\nQuit application and run Programs->On My Disk->Updater manually?"),
                                              QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes;
       }

       if (quit) appQuit();

    }
}

void MainDialog::updateError(const QString &err)
{
    qDebug() << err;
    setStatus(tr("Error while checking for updates."));
}
#endif

MainDialog::~MainDialog()
{
#if WIN32
    delete uManager;
#endif
    delete ui;
}

