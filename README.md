# On My Disk Desktop Client #

Client filesystem backend for On My Disk service

# Authors #
* Alexey Volkov alexey [at] onmydisk.com
* Ivan Kozinov kozinov [at] gmail.com
* Design: Alexender Vasilyev hamster3d [at] gmail.com

# Building & installing #

```
#!/bin/sh
cd src
qmake-qt5 onmydisk.pro
make
sudo make install
```

# VFS engine #
Make sure you have the latest VFS:

```
#!/bin/sh
git subtree pull --prefix=src/vfs --squash git@bitbucket.org:onmydisk/vfs.git master
```

After fixing src/vfs  do not forget to
push the changes back to VFS repo:

```
#!/bin/sh
git subtree push --prefix=src/vfs --squash git@bitbucket.org:onmydisk/vfs.git master
```